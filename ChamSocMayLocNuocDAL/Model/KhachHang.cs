﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChamSocMayLocNuocDAL.Model
{
    public class KhachHang
    {
        [Key]
        public string MaKH { get; set; }

        [StringLength(500)]
        public string TenKH { get; set; }

        [StringLength(500)]
        public string DiaChi { get; set; }

        [StringLength(50)]
        public string SDT { get; set; }

        [StringLength(500)]
        public string GhiChu { get; set; }


    }
}
