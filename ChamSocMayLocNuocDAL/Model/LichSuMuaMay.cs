﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChamSocMayLocNuocDAL.Model
{
   public class LichSuMuaMay
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MaLS { get; set; }

        public string MaKH { get; set; }

        public int MaLoaiMay { get; set; }
        public int MaLoiLoc { get; set; }

        public DateTime NgayMua { get; set;}
      
        public int SoLuong { get; set; }

        [StringLength(500)]
        public string GhiChu { get; set; }
   
    }
}
