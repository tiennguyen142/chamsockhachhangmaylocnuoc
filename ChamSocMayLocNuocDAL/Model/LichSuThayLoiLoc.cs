﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChamSocMayLocNuocDAL.Model
{
    public class LichSuThayLoiLoc
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MaLS { get; set; }
     
        public string MaKH { get; set; }
        public int MaLoiLoc { get; set; }
        public DateTime NgayMua { get; set; }
        public int SoLuong { get; set; }

        public bool DaXem { get; set; }
        public bool DaGoi { get; set; }

        [StringLength(500)]
        public string GhiChu { get; set; }
    }
}
