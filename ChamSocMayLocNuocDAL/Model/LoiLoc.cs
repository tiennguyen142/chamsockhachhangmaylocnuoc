﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChamSocMayLocNuocDAL.Model
{
    public class LoiLoc
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MaLoiLoc { get; set; }
        public int MaLoaiMayLoc { get; set; }
        [StringLength(500)]
        public string TenLoiLoc { get; set; }

        public int ThoiGianThayLoi { get; set; }

        [StringLength(500)]
        public string GhiChu { get; set; }
    
    }
}
