﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity;
namespace ChamSocMayLocNuocDAL.Model
{
    public class ChamSocMayLocNuocDbContext: DbContext
    {
        public ChamSocMayLocNuocDbContext():base("ChamSocKHMayLocNuoc")
        {
            
        }
        public bool checkConnected()
        {
            using (ChamSocMayLocNuocDbContext dbContext = new ChamSocMayLocNuocDbContext())
            {
               return dbContext.Database.Exists();
            }
        }
        public DbSet<KhachHang> KhachHangs { get; set; }
        public DbSet<LichSuMuaMay> LichSuMuaMays { get; set; }
        public DbSet<LichSuThayLoiLoc> LichSuThayLoiLocs { get; set; }
        public DbSet<LoiLoc> LoiLocs { get; set; }
        public DbSet<LoaiMayLocNuoc> LoaiMayLocNuocs { get; set; }



    }
}
