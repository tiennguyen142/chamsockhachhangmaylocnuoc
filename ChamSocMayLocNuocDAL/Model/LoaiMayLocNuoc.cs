﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChamSocMayLocNuocDAL.Model
{
   public class LoaiMayLocNuoc
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MaLoaiMay { get; set; }

        [StringLength(500)]
        public string TenLoaiMay { get; set; }


        [StringLength(500)]
        public string GhiChu { get; set; }

    }
}
