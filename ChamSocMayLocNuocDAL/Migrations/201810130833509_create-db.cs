namespace ChamSocMayLocNuocDAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createdb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.KhachHangs",
                c => new
                    {
                        MaKH = c.String(nullable: false, maxLength: 128),
                        TenKH = c.String(maxLength: 500),
                        DiaChi = c.String(maxLength: 500),
                        SDT = c.String(maxLength: 50),
                        GhiChu = c.String(maxLength: 500),
                    })
                .PrimaryKey(t => t.MaKH);
            
            CreateTable(
                "dbo.LichSuMuaMays",
                c => new
                    {
                        MaLS = c.Int(nullable: false, identity: true),
                        MaKH = c.String(),
                        MaLoaiMay = c.Int(nullable: false),
                        MaLoiLoc = c.Int(nullable: false),
                        NgayMua = c.DateTime(nullable: false),
                        SoLuong = c.Int(nullable: false),
                        GhiChu = c.String(maxLength: 500),
                    })
                .PrimaryKey(t => t.MaLS);
            
            CreateTable(
                "dbo.LichSuThayLoiLocs",
                c => new
                    {
                        MaLS = c.Int(nullable: false, identity: true),
                        MaKH = c.String(),
                        MaLoiLoc = c.Int(nullable: false),
                        NgayMua = c.DateTime(nullable: false),
                        SoLuong = c.Int(nullable: false),
                        GhiChu = c.String(maxLength: 500),
                    })
                .PrimaryKey(t => t.MaLS);
            
            CreateTable(
                "dbo.LoaiMayLocNuocs",
                c => new
                    {
                        MaLoaiMay = c.Int(nullable: false, identity: true),
                        TenLoaiMay = c.String(maxLength: 500),
                        GhiChu = c.String(maxLength: 500),
                    })
                .PrimaryKey(t => t.MaLoaiMay);
            
            CreateTable(
                "dbo.LoiLocs",
                c => new
                    {
                        MaLoiLoc = c.Int(nullable: false, identity: true),
                        MaLoaiMayLoc = c.Int(nullable: false),
                        TenLoiLoc = c.String(maxLength: 500),
                        ThoiGianThayLoi = c.Int(nullable: false),
                        GhiChu = c.String(maxLength: 500),
                    })
                .PrimaryKey(t => t.MaLoiLoc);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.LoiLocs");
            DropTable("dbo.LoaiMayLocNuocs");
            DropTable("dbo.LichSuThayLoiLocs");
            DropTable("dbo.LichSuMuaMays");
            DropTable("dbo.KhachHangs");
        }
    }
}
