namespace ChamSocMayLocNuocDAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addcolumnDaXemDaGoi : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LichSuThayLoiLocs", "DaXem", c => c.Boolean(nullable: false));
            AddColumn("dbo.LichSuThayLoiLocs", "DaGoi", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LichSuThayLoiLocs", "DaGoi");
            DropColumn("dbo.LichSuThayLoiLocs", "DaXem");
        }
    }
}
