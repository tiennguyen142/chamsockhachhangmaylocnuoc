﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ChamSocMayLocNuocDAL.Model;

namespace ChamSocMayLocNuoc
{
    public partial class frmThemLoaiMayLoc : DevExpress.XtraEditors.XtraForm
    {
        ChamSocMayLocNuocDbContext dbc = new ChamSocMayLocNuocDbContext();
        private bool IsEdit = true;
        private int MaMayCapNhat;
        public frmThemLoaiMayLoc()
        {
            InitializeComponent();
            LoadLoaiMay();
        }
        public void SaveLoaiMay()
        {

            DialogResult result = MessageBox.Show("Bạn có chắc muốn thêm một máy lọc \"" + txtTenLM.Text + "\" không?", "Thông Báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (result.Equals(DialogResult.OK))
            {
                try
                {

                    LoaiMayLocNuoc LoaiMay = new LoaiMayLocNuoc();
                    LoaiMay.GhiChu = txtGhiChu.Text;
                    LoaiMay.TenLoaiMay = txtTenLM.Text;
                    dbc.LoaiMayLocNuocs.Add(LoaiMay);
                    dbc.SaveChanges();
                    MessageBox.Show("Thêm mới Loại máy thành công");
                }
                catch
                {
                    MessageBox.Show("Thêm mới Loại máy thất bại");
                }

            }


        }
        public void CapNhatLoaiMay()
        {
            DialogResult result = MessageBox.Show("Bạn có chắc muốn cập nhật máy lọc \"" + txtTenLM.Text + "\" không?", "Thông Báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (result.Equals(DialogResult.OK))
            {
                    //Do something
                    try
                    {
                        LoaiMayLocNuoc LoaiMay = new LoaiMayLocNuoc();
                        LoaiMay = dbc.LoaiMayLocNuocs.Where(s => s.MaLoaiMay == MaMayCapNhat).First() as LoaiMayLocNuoc;
                        LoaiMay.GhiChu = txtGhiChu.Text;
                        LoaiMay.TenLoaiMay = txtTenLM.Text;
                        //    dbc.Entry(LoaiMay).State = System.Data.Entity.EntityState.Modified;

                        dbc.SaveChanges();
                        MessageBox.Show("Cập nhật loại máy thành công");
                    }
                    catch
                    {
                        MessageBox.Show("Cập nhật loại máy thất bại");
                    }


                }
              else
            {
      
            }
           

        }
        private void btnLuu_Click(object sender, EventArgs e)
        {

        }
        public void LoadLoaiMay()
        {
            grdLSBH.DataSource = dbc.LoaiMayLocNuocs.ToList();
        }

      
        private void gvLSBH_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            LoaiMayLocNuoc LoaiMayLN = new LoaiMayLocNuoc();
            LoaiMayLN = gvLSBH.GetRow(e.RowHandle) as LoaiMayLocNuoc;
            txtTenLM.Text = LoaiMayLN.TenLoaiMay;
            txtGhiChu.Text = LoaiMayLN.GhiChu;
            MaMayCapNhat = LoaiMayLN.MaLoaiMay;
            IsEdit = true;
        }

        private void btnThemMoi_Click(object sender, EventArgs e)
        {
            txtTenLM.Text = "";
            txtGhiChu.Text = "";
            IsEdit = false;
        }

        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            // Check selected item
            if (txtTenLM.Text != "")
            {
                if (IsEdit)
                {
                    CapNhatLoaiMay();
                }
                else
                {
                    SaveLoaiMay();
                }
                LoadLoaiMay();
                cleanForm();
            }
            else
            {
                MessageBox.Show("Bạn chưa chọn máy lọc nước muốn cập nhật hoặc chưa nhập tên máy lọc mới!");
                txtTenLM.Focus();
            }
        }
        private void cleanForm()
        {
            txtTenLM.Text = "";
            txtGhiChu.Text = "";
            cbThemMayMoi.Checked = false;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (cbThemMayMoi.Checked)
            {
                IsEdit = false;
            }
            else
            {
                IsEdit = true;
            }

            LoadLoaiMay();
        }
    }
}