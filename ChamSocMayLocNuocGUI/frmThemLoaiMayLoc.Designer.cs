﻿namespace ChamSocMayLocNuoc
{
    partial class frmThemLoaiMayLoc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lb_name = new System.Windows.Forms.Label();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.btnCapNhat = new DevExpress.XtraEditors.SimpleButton();
            this.grdLSBH = new DevExpress.XtraGrid.GridControl();
            this.gvLSBH = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gvcolMaLoaiMay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolTenLoaiMay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvGhiChu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnThietLap = new DevExpress.XtraEditors.SimpleButton();
            this.txtGhiChu = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTenLM = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
			this.cbThemMayMoi = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdLSBH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLSBH)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Controls.Add(this.lb_name);
            this.panelControl1.Location = new System.Drawing.Point(-2, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(987, 38);
            this.panelControl1.TabIndex = 14;
            // 
            // lb_name
            // 
            this.lb_name.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_name.Location = new System.Drawing.Point(0, 4);
            this.lb_name.Name = "lb_name";
            this.lb_name.Size = new System.Drawing.Size(982, 26);
            this.lb_name.TabIndex = 1;
            this.lb_name.Text = "Loại Máy Nước Lọc";
            this.lb_name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.Controls.Add(this.grdLSBH);
            this.panelControl2.Controls.Add(this.btnThietLap);
            this.panelControl2.Location = new System.Drawing.Point(-2, 169);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(987, 391);
            this.panelControl2.TabIndex = 14;
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapNhat.Appearance.Options.UseFont = true;
            this.btnCapNhat.Location = new System.Drawing.Point(811, 81);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(114, 49);
            this.btnCapNhat.TabIndex = 29;
            this.btnCapNhat.Text = "Lưu";
            this.btnCapNhat.Click += new System.EventHandler(this.btnCapNhat_Click);
            // 
            // grdLSBH
            // 
            this.grdLSBH.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.grdLSBH.Location = new System.Drawing.Point(5, 7);
            this.grdLSBH.MainView = this.gvLSBH;
            this.grdLSBH.Name = "grdLSBH";
            this.grdLSBH.Size = new System.Drawing.Size(977, 384);
            this.grdLSBH.TabIndex = 28;
            this.grdLSBH.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvLSBH});
            // 
            // gvLSBH
            // 
            this.gvLSBH.ActiveFilterEnabled = false;
            this.gvLSBH.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gvLSBH.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvLSBH.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gvcolMaLoaiMay,
            this.gcolTenLoaiMay,
            this.gvGhiChu});
            this.gvLSBH.GridControl = this.grdLSBH;
            this.gvLSBH.Name = "gvLSBH";
            this.gvLSBH.OptionsCustomization.AllowFilter = false;
            this.gvLSBH.OptionsFilter.AllowAutoFilterConditionChange = DevExpress.Utils.DefaultBoolean.True;
            this.gvLSBH.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gvLSBH.OptionsFilter.AllowFilterEditor = false;
            this.gvLSBH.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gvLSBH.OptionsFilter.AllowMRUFilterList = false;
            this.gvLSBH.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gvLSBH.OptionsFilter.ColumnFilterPopupMode = DevExpress.XtraGrid.Columns.ColumnFilterPopupMode.Classic;
            this.gvLSBH.OptionsFilter.DefaultFilterEditorView = DevExpress.XtraEditors.FilterEditorViewMode.Text;
            this.gvLSBH.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gvLSBH.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gvLSBH.OptionsFilter.ShowAllTableValuesInFilterPopup = true;
            this.gvLSBH.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gvLSBH.OptionsView.ShowAutoFilterRow = true;
            this.gvLSBH.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gvLSBH.OptionsView.ShowGroupPanel = false;
            this.gvLSBH.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gvLSBH_RowClick);
            // 
            // gvcolMaLoaiMay
            // 
            this.gvcolMaLoaiMay.Caption = "Mã Loại Máy";
            this.gvcolMaLoaiMay.FieldName = "MaLoaiMay";
            this.gvcolMaLoaiMay.Name = "gvcolMaLoaiMay";
            this.gvcolMaLoaiMay.OptionsColumn.AllowEdit = false;
            this.gvcolMaLoaiMay.Visible = true;
            this.gvcolMaLoaiMay.VisibleIndex = 0;
            this.gvcolMaLoaiMay.Width = 131;
            // 
            // gcolTenLoaiMay
            // 
            this.gcolTenLoaiMay.Caption = "Tên Loại Máy";
            this.gcolTenLoaiMay.FieldName = "TenLoaiMay";
            this.gcolTenLoaiMay.Name = "gcolTenLoaiMay";
            this.gcolTenLoaiMay.OptionsColumn.AllowEdit = false;
            this.gcolTenLoaiMay.Visible = true;
            this.gcolTenLoaiMay.VisibleIndex = 1;
            this.gcolTenLoaiMay.Width = 440;
            // 
            // gvGhiChu
            // 
            this.gvGhiChu.Caption = "Ghi Chú";
            this.gvGhiChu.FieldName = "GhiChu";
            this.gvGhiChu.Name = "gvGhiChu";
            this.gvGhiChu.Visible = true;
            this.gvGhiChu.VisibleIndex = 2;
            this.gvGhiChu.Width = 217;
            // 
            // btnThietLap
            // 
            this.btnThietLap.Location = new System.Drawing.Point(347, 362);
            this.btnThietLap.Name = "btnThietLap";
            this.btnThietLap.Size = new System.Drawing.Size(114, 39);
            this.btnThietLap.TabIndex = 27;
            this.btnThietLap.Text = "Lưu";
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Location = new System.Drawing.Point(153, 81);
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(636, 49);
            this.txtGhiChu.TabIndex = 25;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(69, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 23);
            this.label2.TabIndex = 23;
            this.label2.Text = "Ghi Chú :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTenLM
            // 
            this.txtTenLM.Location = new System.Drawing.Point(153, 53);
            this.txtTenLM.Name = "txtTenLM";
            this.txtTenLM.Size = new System.Drawing.Size(636, 21);
            this.txtTenLM.TabIndex = 25;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(31, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 23);
            this.label1.TabIndex = 23;
            this.label1.Text = "Tên Loại Máy :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbThemMayMoi
            // 
            this.cbThemMayMoi.AutoSize = true;
            this.cbThemMayMoi.Location = new System.Drawing.Point(153, 136);
            this.cbThemMayMoi.Name = "cbThemMayMoi";
            this.cbThemMayMoi.Size = new System.Drawing.Size(94, 17);
            this.cbThemMayMoi.TabIndex = 30;
            this.cbThemMayMoi.Text = "Thêm máy mới";
            this.cbThemMayMoi.UseVisualStyleBackColor = true;
            this.cbThemMayMoi.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // frmThemLoaiMayLoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.cbThemMayMoi);
            this.Controls.Add(this.btnCapNhat);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
			this.Controls.Add(this.txtTenLM);
            this.Controls.Add(this.txtGhiChu);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Name = "frmThemLoaiMayLoc";
            this.ShowIcon = false;
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdLSBH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLSBH)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.Label lb_name;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btnThietLap;
        private System.Windows.Forms.TextBox txtTenLM;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtGhiChu;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraGrid.GridControl grdLSBH;
        private DevExpress.XtraGrid.Views.Grid.GridView gvLSBH;
        private DevExpress.XtraGrid.Columns.GridColumn gvcolMaLoaiMay;
        private DevExpress.XtraGrid.Columns.GridColumn gcolTenLoaiMay;
        private DevExpress.XtraGrid.Columns.GridColumn gvGhiChu;
        private DevExpress.XtraEditors.SimpleButton btnCapNhat;
        private System.Windows.Forms.CheckBox cbThemMayMoi;
    }
}