﻿namespace ChamSocMayLocNuoc
{
    partial class frmThemKhachHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmThemKhachHang));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lb_name = new System.Windows.Forms.Label();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.btnXoaThongTin = new DevExpress.XtraEditors.SimpleButton();
            this.cbThemKhachHang = new System.Windows.Forms.CheckBox();
            this.txtGhiChu = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.grdTCKH = new DevExpress.XtraGrid.GridControl();
            this.GvTCKH = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcolMaKH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolTenKH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolSDT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolSP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvColTenLoi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolNMua = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gcolTHBH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grThayGianThayThe = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.cldNgayMua = new DevExpress.XtraEditors.DateEdit();
            this.chkcbLoaiLoiLoc = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.chkcbLoaiMay = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.txtDiaChi = new System.Windows.Forms.TextBox();
            this.txtSDT = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lbDiaChi = new System.Windows.Forms.Label();
            this.lbLoaiMay = new System.Windows.Forms.Label();
            this.lbSDT = new System.Windows.Forms.Label();
            this.txtMaKh = new System.Windows.Forms.TextBox();
            this.txtTenKH = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lbNgayMua = new System.Windows.Forms.Label();
            this.lbTenKH = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdTCKH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GvTCKH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grThayGianThayThe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cldNgayMua.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cldNgayMua.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkcbLoaiLoiLoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkcbLoaiMay.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Controls.Add(this.lb_name);
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1184, 41);
            this.panelControl1.TabIndex = 0;
            // 
            // lb_name
            // 
            this.lb_name.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_name.Location = new System.Drawing.Point(0, 2);
            this.lb_name.Name = "lb_name";
            this.lb_name.Size = new System.Drawing.Size(1184, 34);
            this.lb_name.TabIndex = 0;
            this.lb_name.Text = "Thêm / Sửa Khách Hàng";
            this.lb_name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.Controls.Add(this.btnXoaThongTin);
            this.panelControl2.Controls.Add(this.cbThemKhachHang);
            this.panelControl2.Controls.Add(this.txtGhiChu);
            this.panelControl2.Controls.Add(this.label3);
            this.panelControl2.Controls.Add(this.grdTCKH);
            this.panelControl2.Controls.Add(this.cldNgayMua);
            this.panelControl2.Controls.Add(this.chkcbLoaiLoiLoc);
            this.panelControl2.Controls.Add(this.chkcbLoaiMay);
            this.panelControl2.Controls.Add(this.btnLuu);
            this.panelControl2.Controls.Add(this.txtDiaChi);
            this.panelControl2.Controls.Add(this.txtSDT);
            this.panelControl2.Controls.Add(this.label1);
            this.panelControl2.Controls.Add(this.lbDiaChi);
            this.panelControl2.Controls.Add(this.lbLoaiMay);
            this.panelControl2.Controls.Add(this.lbSDT);
            this.panelControl2.Controls.Add(this.txtMaKh);
            this.panelControl2.Controls.Add(this.txtTenKH);
            this.panelControl2.Controls.Add(this.label2);
            this.panelControl2.Controls.Add(this.lbNgayMua);
            this.panelControl2.Controls.Add(this.lbTenKH);
            this.panelControl2.Location = new System.Drawing.Point(0, 39);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1184, 622);
            this.panelControl2.TabIndex = 0;
            // 
            // btnXoaThongTin
            // 
            this.btnXoaThongTin.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaThongTin.Appearance.Options.UseFont = true;
            this.btnXoaThongTin.Location = new System.Drawing.Point(1058, 64);
            this.btnXoaThongTin.Name = "btnXoaThongTin";
            this.btnXoaThongTin.Size = new System.Drawing.Size(114, 53);
            this.btnXoaThongTin.TabIndex = 12;
            this.btnXoaThongTin.Text = "Xóa thông tin";
            this.btnXoaThongTin.Click += new System.EventHandler(this.btnXoaThongTin_Click);
            // 
            // cbThemKhachHang
            // 
            this.cbThemKhachHang.AutoSize = true;
            this.cbThemKhachHang.Location = new System.Drawing.Point(181, 198);
            this.cbThemKhachHang.Name = "cbThemKhachHang";
            this.cbThemKhachHang.Size = new System.Drawing.Size(129, 17);
            this.cbThemKhachHang.TabIndex = 11;
            this.cbThemKhachHang.Text = "Thêm khách hàng mới";
            this.cbThemKhachHang.UseVisualStyleBackColor = true;
            this.cbThemKhachHang.CheckedChanged += new System.EventHandler(this.cbThemKhachHang_CheckedChanged);
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Location = new System.Drawing.Point(709, 118);
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(326, 65);
            this.txtGhiChu.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(622, 124);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 23);
            this.label3.TabIndex = 9;
            this.label3.Text = "Ghi Chú:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // grdTCKH
            // 
            this.grdTCKH.Location = new System.Drawing.Point(3, 238);
            this.grdTCKH.MainView = this.GvTCKH;
            this.grdTCKH.Name = "grdTCKH";
            this.grdTCKH.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.grThayGianThayThe,
            this.repositoryItemDateEdit1,
            this.repositoryItemDateEdit2});
            this.grdTCKH.Size = new System.Drawing.Size(1176, 401);
            this.grdTCKH.TabIndex = 8;
            this.grdTCKH.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GvTCKH});
            // 
            // GvTCKH
            // 
            this.GvTCKH.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcolMaKH,
            this.gcolTenKH,
            this.gcolSDT,
            this.gcolSP,
            this.gvColTenLoi,
            this.gcolNMua,
            this.gcolTHBH});
            this.GvTCKH.GridControl = this.grdTCKH;
            this.GvTCKH.Name = "GvTCKH";
            this.GvTCKH.OptionsBehavior.Editable = false;
            this.GvTCKH.OptionsBehavior.ReadOnly = true;
            this.GvTCKH.OptionsView.ShowAutoFilterRow = true;
            this.GvTCKH.OptionsView.ShowGroupPanel = false;
            this.GvTCKH.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.GvTCKH_RowClick);
            this.GvTCKH.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.GvTCKH_CustomDrawRowIndicator);
            // 
            // gcolMaKH
            // 
            this.gcolMaKH.Caption = "Mã Khách Hàng";
            this.gcolMaKH.FieldName = "MaKH";
            this.gcolMaKH.MinWidth = 10;
            this.gcolMaKH.Name = "gcolMaKH";
            this.gcolMaKH.OptionsColumn.AllowEdit = false;
            this.gcolMaKH.Visible = true;
            this.gcolMaKH.VisibleIndex = 0;
            this.gcolMaKH.Width = 125;
            // 
            // gcolTenKH
            // 
            this.gcolTenKH.Caption = "Tên Khách Hàng";
            this.gcolTenKH.FieldName = "TenKH";
            this.gcolTenKH.MinWidth = 40;
            this.gcolTenKH.Name = "gcolTenKH";
            this.gcolTenKH.Visible = true;
            this.gcolTenKH.VisibleIndex = 1;
            this.gcolTenKH.Width = 143;
            // 
            // gcolSDT
            // 
            this.gcolSDT.Caption = "Số Điện Thoại";
            this.gcolSDT.FieldName = "SDT";
            this.gcolSDT.MinWidth = 10;
            this.gcolSDT.Name = "gcolSDT";
            this.gcolSDT.Visible = true;
            this.gcolSDT.VisibleIndex = 2;
            this.gcolSDT.Width = 122;
            // 
            // gcolSP
            // 
            this.gcolSP.Caption = "Tên Loại Máy";
            this.gcolSP.FieldName = "TenLoaiMay";
            this.gcolSP.Name = "gcolSP";
            this.gcolSP.Visible = true;
            this.gcolSP.VisibleIndex = 3;
            this.gcolSP.Width = 99;
            // 
            // gvColTenLoi
            // 
            this.gvColTenLoi.Caption = "Tên Lõi";
            this.gvColTenLoi.FieldName = "TenLoiLoc";
            this.gvColTenLoi.Name = "gvColTenLoi";
            this.gvColTenLoi.Visible = true;
            this.gvColTenLoi.VisibleIndex = 4;
            this.gvColTenLoi.Width = 90;
            // 
            // gcolNMua
            // 
            this.gcolNMua.Caption = "Ngày Mua";
            this.gcolNMua.ColumnEdit = this.repositoryItemDateEdit2;
            this.gcolNMua.FieldName = "NgayMua";
            this.gcolNMua.MinWidth = 10;
            this.gcolNMua.Name = "gcolNMua";
            this.gcolNMua.Visible = true;
            this.gcolNMua.VisibleIndex = 5;
            this.gcolNMua.Width = 119;
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit2.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit2.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit2.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            // 
            // gcolTHBH
            // 
            this.gcolTHBH.Caption = "Thời Gian Thay Thế Lõi";
            this.gcolTHBH.ColumnEdit = this.grThayGianThayThe;
            this.gcolTHBH.FieldName = "ThoiGianThayThe";
            this.gcolTHBH.MinWidth = 10;
            this.gcolTHBH.Name = "gcolTHBH";
            this.gcolTHBH.Visible = true;
            this.gcolTHBH.VisibleIndex = 6;
            this.gcolTHBH.Width = 124;
            // 
            // grThayGianThayThe
            // 
            this.grThayGianThayThe.AutoHeight = false;
            this.grThayGianThayThe.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.grThayGianThayThe.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Thời gian thay lõi"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Mã")});
            this.grThayGianThayThe.DisplayMember = "Name";
            this.grThayGianThayThe.KeyMember = "Code";
            this.grThayGianThayThe.Name = "grThayGianThayThe";
            this.grThayGianThayThe.NullText = "";
            this.grThayGianThayThe.ReadOnly = true;
            this.grThayGianThayThe.ValueMember = "Code";
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // cldNgayMua
            // 
            this.cldNgayMua.EditValue = null;
            this.cldNgayMua.Location = new System.Drawing.Point(709, 14);
            this.cldNgayMua.Name = "cldNgayMua";
            this.cldNgayMua.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cldNgayMua.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cldNgayMua.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.cldNgayMua.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.cldNgayMua.Properties.EditFormat.FormatString = "";
            this.cldNgayMua.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.cldNgayMua.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.cldNgayMua.Size = new System.Drawing.Size(326, 20);
            this.cldNgayMua.TabIndex = 7;
            // 
            // chkcbLoaiLoiLoc
            // 
            this.chkcbLoaiLoiLoc.EditValue = ((object)(resources.GetObject("chkcbLoaiLoiLoc.EditValue")));
            this.chkcbLoaiLoiLoc.Location = new System.Drawing.Point(709, 82);
            this.chkcbLoaiLoiLoc.Name = "chkcbLoaiLoiLoc";
            this.chkcbLoaiLoiLoc.Properties.AllowMultiSelect = true;
            this.chkcbLoaiLoiLoc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.chkcbLoaiLoiLoc.Properties.DisplayMember = "TenLoiLoc";
            this.chkcbLoaiLoiLoc.Properties.EditValueType = DevExpress.XtraEditors.Repository.EditValueTypeCollection.List;
            this.chkcbLoaiLoiLoc.Properties.NullText = "Vui lòng chọn chủng loại";
            this.chkcbLoaiLoiLoc.Properties.NullValuePrompt = "Vui lòng chọn chủng loại";
            this.chkcbLoaiLoiLoc.Properties.NullValuePromptShowForEmptyValue = true;
            this.chkcbLoaiLoiLoc.Properties.SelectAllItemCaption = "Select All";
            this.chkcbLoaiLoiLoc.Properties.SelectAllItemVisible = false;
            this.chkcbLoaiLoiLoc.Properties.ValueMember = "MaLoiLoc";
            this.chkcbLoaiLoiLoc.Size = new System.Drawing.Size(326, 20);
            this.chkcbLoaiLoiLoc.TabIndex = 6;
            // 
            // chkcbLoaiMay
            // 
            this.chkcbLoaiMay.EditValue = ((object)(resources.GetObject("chkcbLoaiMay.EditValue")));
            this.chkcbLoaiMay.Location = new System.Drawing.Point(181, 86);
            this.chkcbLoaiMay.Name = "chkcbLoaiMay";
            this.chkcbLoaiMay.Properties.AllowMultiSelect = true;
            this.chkcbLoaiMay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.chkcbLoaiMay.Properties.DisplayMember = "TenLoaiMay";
            this.chkcbLoaiMay.Properties.EditValueType = DevExpress.XtraEditors.Repository.EditValueTypeCollection.List;
            this.chkcbLoaiMay.Properties.NullText = "Vui lòng chọn chủng loại";
            this.chkcbLoaiMay.Properties.NullValuePrompt = "Vui lòng chọn chủng loại";
            this.chkcbLoaiMay.Properties.NullValuePromptShowForEmptyValue = true;
            this.chkcbLoaiMay.Properties.SelectAllItemCaption = "Select All";
            this.chkcbLoaiMay.Properties.SelectAllItemVisible = false;
            this.chkcbLoaiMay.Properties.ValueMember = "MaLoaiMay";
            this.chkcbLoaiMay.Size = new System.Drawing.Size(369, 20);
            this.chkcbLoaiMay.TabIndex = 6;
            this.chkcbLoaiMay.EditValueChanged += new System.EventHandler(this.chkcbLoaiMay_EditValueChanged);
            // 
            // btnLuu
            // 
            this.btnLuu.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Appearance.Options.UseFont = true;
            this.btnLuu.Location = new System.Drawing.Point(1058, 130);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(114, 53);
            this.btnLuu.TabIndex = 5;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Location = new System.Drawing.Point(181, 118);
            this.txtDiaChi.Multiline = true;
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(369, 65);
            this.txtDiaChi.TabIndex = 1;
            // 
            // txtSDT
            // 
            this.txtSDT.Location = new System.Drawing.Point(709, 49);
            this.txtSDT.Name = "txtSDT";
            this.txtSDT.Size = new System.Drawing.Size(326, 21);
            this.txtSDT.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(622, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Loại lỏi :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbDiaChi
            // 
            this.lbDiaChi.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDiaChi.Location = new System.Drawing.Point(86, 124);
            this.lbDiaChi.Name = "lbDiaChi";
            this.lbDiaChi.Size = new System.Drawing.Size(66, 23);
            this.lbDiaChi.TabIndex = 0;
            this.lbDiaChi.Text = "Địa chỉ:";
            this.lbDiaChi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbLoaiMay
            // 
            this.lbLoaiMay.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLoaiMay.Location = new System.Drawing.Point(65, 85);
            this.lbLoaiMay.Name = "lbLoaiMay";
            this.lbLoaiMay.Size = new System.Drawing.Size(84, 23);
            this.lbLoaiMay.TabIndex = 0;
            this.lbLoaiMay.Text = "Loại máy :";
            this.lbLoaiMay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbSDT
            // 
            this.lbSDT.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSDT.Location = new System.Drawing.Point(597, 45);
            this.lbSDT.Name = "lbSDT";
            this.lbSDT.Size = new System.Drawing.Size(106, 23);
            this.lbSDT.TabIndex = 0;
            this.lbSDT.Text = "Điện Thoại :";
            this.lbSDT.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMaKh
            // 
            this.txtMaKh.Location = new System.Drawing.Point(180, 14);
            this.txtMaKh.Name = "txtMaKh";
            this.txtMaKh.ReadOnly = true;
            this.txtMaKh.Size = new System.Drawing.Size(370, 21);
            this.txtMaKh.TabIndex = 1;
            // 
            // txtTenKH
            // 
            this.txtTenKH.Location = new System.Drawing.Point(180, 49);
            this.txtTenKH.Name = "txtTenKH";
            this.txtTenKH.Size = new System.Drawing.Size(370, 21);
            this.txtTenKH.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(21, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 23);
            this.label2.TabIndex = 0;
            this.label2.Text = "Mã Khách Hàng :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbNgayMua
            // 
            this.lbNgayMua.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNgayMua.Location = new System.Drawing.Point(606, 11);
            this.lbNgayMua.Name = "lbNgayMua";
            this.lbNgayMua.Size = new System.Drawing.Size(100, 23);
            this.lbNgayMua.TabIndex = 0;
            this.lbNgayMua.Text = "Ngày Mua :";
            this.lbNgayMua.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbTenKH
            // 
            this.lbTenKH.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTenKH.Location = new System.Drawing.Point(12, 45);
            this.lbTenKH.Name = "lbTenKH";
            this.lbTenKH.Size = new System.Drawing.Size(140, 23);
            this.lbTenKH.TabIndex = 0;
            this.lbTenKH.Text = "Tên Khách Hàng :";
            this.lbTenKH.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmThemKhachHang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 661);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmThemKhachHang";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdTCKH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GvTCKH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grThayGianThayThe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cldNgayMua.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cldNgayMua.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkcbLoaiLoiLoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkcbLoaiMay.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.Label lb_name;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private System.Windows.Forms.TextBox txtSDT;
        private System.Windows.Forms.Label lbSDT;
        private System.Windows.Forms.TextBox txtTenKH;
        private System.Windows.Forms.Label lbNgayMua;
        private System.Windows.Forms.Label lbTenKH;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private System.Windows.Forms.Label lbLoaiMay;
        private DevExpress.XtraEditors.CheckedComboBoxEdit chkcbLoaiMay;
        private System.Windows.Forms.TextBox txtDiaChi;
        private System.Windows.Forms.Label lbDiaChi;
        private DevExpress.XtraEditors.CheckedComboBoxEdit chkcbLoaiLoiLoc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtMaKh;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.DateEdit cldNgayMua;
        private DevExpress.XtraGrid.GridControl grdTCKH;
        private DevExpress.XtraGrid.Views.Grid.GridView GvTCKH;
        private DevExpress.XtraGrid.Columns.GridColumn gcolMaKH;
        private DevExpress.XtraGrid.Columns.GridColumn gcolTenKH;
        private DevExpress.XtraGrid.Columns.GridColumn gcolSDT;
        private DevExpress.XtraGrid.Columns.GridColumn gcolSP;
        private DevExpress.XtraGrid.Columns.GridColumn gvColTenLoi;
        private DevExpress.XtraGrid.Columns.GridColumn gcolNMua;
        private DevExpress.XtraGrid.Columns.GridColumn gcolTHBH;
        public DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit grThayGianThayThe;
        private System.Windows.Forms.CheckBox cbThemKhachHang;
        private System.Windows.Forms.TextBox txtGhiChu;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.SimpleButton btnXoaThongTin;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
    }
}