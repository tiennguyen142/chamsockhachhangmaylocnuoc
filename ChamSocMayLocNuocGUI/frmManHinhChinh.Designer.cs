﻿namespace ChamSocMayLocNuoc
{
    partial class frmManHinhChinh
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmManHinhChinh));
            this.plManHinhChinh = new DevExpress.XtraEditors.PanelControl();
            this.btnNhacNho = new System.Windows.Forms.Button();
            this.btnThemLoiLoc = new System.Windows.Forms.Button();
            this.btnTSP = new System.Windows.Forms.Button();
            this.btnTraCuuKH = new System.Windows.Forms.Button();
            this.btnThemKH = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.plManHinhChinh)).BeginInit();
            this.plManHinhChinh.SuspendLayout();
            this.SuspendLayout();
            // 
            // plManHinhChinh
            // 
            this.plManHinhChinh.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.plManHinhChinh.Appearance.BackColor = System.Drawing.Color.White;
            this.plManHinhChinh.Appearance.Options.UseBackColor = true;
            this.plManHinhChinh.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.plManHinhChinh.Controls.Add(this.btnNhacNho);
            this.plManHinhChinh.Controls.Add(this.btnThemLoiLoc);
            this.plManHinhChinh.Controls.Add(this.btnTSP);
            this.plManHinhChinh.Controls.Add(this.btnTraCuuKH);
            this.plManHinhChinh.Controls.Add(this.btnThemKH);
            this.plManHinhChinh.Controls.Add(this.label7);
            this.plManHinhChinh.Controls.Add(this.label2);
            this.plManHinhChinh.Location = new System.Drawing.Point(0, 1);
            this.plManHinhChinh.Name = "plManHinhChinh";
            this.plManHinhChinh.Size = new System.Drawing.Size(1101, 643);
            this.plManHinhChinh.TabIndex = 0;
            // 
            // btnNhacNho
            // 
            this.btnNhacNho.BackColor = System.Drawing.Color.Transparent;
            this.btnNhacNho.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNhacNho.BackgroundImage")));
            this.btnNhacNho.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNhacNho.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNhacNho.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btnNhacNho.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.btnNhacNho.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNhacNho.Location = new System.Drawing.Point(1020, 13);
            this.btnNhacNho.Name = "btnNhacNho";
            this.btnNhacNho.Size = new System.Drawing.Size(69, 66);
            this.btnNhacNho.TabIndex = 7;
            this.btnNhacNho.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnNhacNho.UseVisualStyleBackColor = false;
            this.btnNhacNho.Click += new System.EventHandler(this.btnNhacNho_Click);
            // 
            // btnThemLoiLoc
            // 
            this.btnThemLoiLoc.BackColor = System.Drawing.Color.White;
            this.btnThemLoiLoc.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnThemLoiLoc.BackgroundImage")));
            this.btnThemLoiLoc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnThemLoiLoc.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnThemLoiLoc.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btnThemLoiLoc.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.btnThemLoiLoc.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnThemLoiLoc.Location = new System.Drawing.Point(621, 364);
            this.btnThemLoiLoc.Name = "btnThemLoiLoc";
            this.btnThemLoiLoc.Size = new System.Drawing.Size(212, 209);
            this.btnThemLoiLoc.TabIndex = 6;
            this.btnThemLoiLoc.Text = "THÊM LOẠI LÕI LỌC";
            this.btnThemLoiLoc.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnThemLoiLoc.UseVisualStyleBackColor = false;
            this.btnThemLoiLoc.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnTSP
            // 
            this.btnTSP.BackColor = System.Drawing.Color.White;
            this.btnTSP.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnTSP.BackgroundImage")));
            this.btnTSP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTSP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTSP.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btnTSP.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.btnTSP.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTSP.Location = new System.Drawing.Point(301, 364);
            this.btnTSP.Name = "btnTSP";
            this.btnTSP.Size = new System.Drawing.Size(212, 209);
            this.btnTSP.TabIndex = 5;
            this.btnTSP.Text = "THÊM LOẠI MÁY LỌC";
            this.btnTSP.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnTSP.UseVisualStyleBackColor = false;
            this.btnTSP.Click += new System.EventHandler(this.btnTSP_Click);
            // 
            // btnTraCuuKH
            // 
            this.btnTraCuuKH.BackColor = System.Drawing.Color.LightGray;
            this.btnTraCuuKH.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnTraCuuKH.BackgroundImage")));
            this.btnTraCuuKH.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTraCuuKH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTraCuuKH.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btnTraCuuKH.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.btnTraCuuKH.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTraCuuKH.Location = new System.Drawing.Point(621, 98);
            this.btnTraCuuKH.Name = "btnTraCuuKH";
            this.btnTraCuuKH.Size = new System.Drawing.Size(212, 209);
            this.btnTraCuuKH.TabIndex = 4;
            this.btnTraCuuKH.Text = "TRA CỨU KHÁCH HÀNG";
            this.btnTraCuuKH.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnTraCuuKH.UseVisualStyleBackColor = false;
            this.btnTraCuuKH.Click += new System.EventHandler(this.btnTraCuuKH_Click);
            // 
            // btnThemKH
            // 
            this.btnThemKH.BackColor = System.Drawing.Color.LightGray;
            this.btnThemKH.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnThemKH.BackgroundImage")));
            this.btnThemKH.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnThemKH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnThemKH.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btnThemKH.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.btnThemKH.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnThemKH.Location = new System.Drawing.Point(301, 98);
            this.btnThemKH.Name = "btnThemKH";
            this.btnThemKH.Size = new System.Drawing.Size(212, 209);
            this.btnThemKH.TabIndex = 3;
            this.btnThemKH.Text = "THÊM KHÁCH HÀNG";
            this.btnThemKH.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnThemKH.UseVisualStyleBackColor = false;
            this.btnThemKH.Click += new System.EventHandler(this.btnThemKH_Click);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(1021, 82);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 22);
            this.label7.TabIndex = 2;
            this.label7.Text = "Nhắc nhở";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(184, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(765, 39);
            this.label2.TabIndex = 1;
            this.label2.Text = "Màn Hình Chính";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmManHinhChinh
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1101, 646);
            this.Controls.Add(this.plManHinhChinh);
            this.FormBorderEffect = DevExpress.XtraEditors.FormBorderEffect.Shadow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmManHinhChinh";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Phần mềm chăm sóc khách hàng máy lọc nước";
            ((System.ComponentModel.ISupportInitialize)(this.plManHinhChinh)).EndInit();
            this.plManHinhChinh.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl plManHinhChinh;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnThemKH;
        private System.Windows.Forms.Button btnTraCuuKH;
        private System.Windows.Forms.Button btnTSP;
        private System.Windows.Forms.Button btnNhacNho;
        private System.Windows.Forms.Button btnThemLoiLoc;
    }
}

