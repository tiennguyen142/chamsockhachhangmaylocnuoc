﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ChamSocMayLocNuocDAL.Model;
using ChamSocMayLocNuoc.Model;
using System.Globalization;

namespace ChamSocMayLocNuoc
{
    public partial class frmThemLichSuThayLoiLoc : DevExpress.XtraEditors.XtraForm
    {
        ChamSocMayLocNuocDbContext dbc = new ChamSocMayLocNuocDbContext();
        //public TraCuuKhachHang MaKhobj = new TraCuuKhachHang();
        
        public frmThemLichSuThayLoiLoc()
        {
            InitializeComponent();
           // LoadCombobox();
        }
        public frmThemLichSuThayLoiLoc(TraCuuKhachHang MaKhobj):this()
        {
            
            LoadData(MaKhobj);
            LoadCombobox();
        }
        public void SaveData()
        {
            try
            {
                List<LoiLoc> tempLoiLoc = new List<LoiLoc>();
                foreach (var Item in chkbLoaiLoi.Properties.GetItems().Where(a => a.CheckState == CheckState.Checked))
                {
                    LichSuThayLoiLoc lsloiLoc = new LichSuThayLoiLoc();
                    lsloiLoc.MaKH = txtMaKH.Text;
                    lsloiLoc.GhiChu = txtGhiChu.Text;
                    lsloiLoc.NgayMua = DateTime.ParseExact(string.IsNullOrEmpty(cldNgayMua.Text) ? "00-00-0000" : cldNgayMua.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    lsloiLoc.MaLoiLoc = int.Parse(Item.Value.ToString());
                    dbc.LichSuThayLoiLocs.Add(lsloiLoc);
                    
                   // tempLoiLoc.AddRange(dbc.LoiLocs.Where(a => a.MaLoaiMayLoc == int.Parse(Item.Value.ToString())));
                }
                //chkbLoaiLoi.Properties.DataSource = tempLoiLoc;
                
                dbc.SaveChanges();
                MessageBox.Show("Lưu Thành Công");
            }
            catch 
            {
                MessageBox.Show("Lưu Thất Bại");
            }
          
        }
        public void LoadCombobox()
        {
            //List<LoiLoc> LoiLoclst = new List<LoiLoc>();
            //foreach (var item in dbc.LoiLocs.ToList())
            //{
            //    item.TenLoiLoc = item.TenLoiLoc + " ( " + item.ThoiGianThayLoi + " Tháng )";
            //    LoiLoclst.Add(item);
            //}
            
            chkbLoaiLoi.Properties.DataSource = dbc.LoiLocs.ToList();
        }
        public void LoadData(TraCuuKhachHang MaKhobj)
        {
            txtTenKH.Text =  MaKhobj.TenKH;
            txtMaKH.Text = MaKhobj.MaKH;

        }
        private void btnLuu_Click(object sender, EventArgs e)
        {
            SaveData();
        }
    }
}