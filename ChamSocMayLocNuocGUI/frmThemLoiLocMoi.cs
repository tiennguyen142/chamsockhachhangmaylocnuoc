﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ChamSocMayLocNuocDAL.Model;
using ChamSocMayLocNuoc.Model;
namespace ChamSocMayLocNuoc
{
    public partial class frmThemLoiLocMoi : DevExpress.XtraEditors.XtraForm
    {
        ChamSocMayLocNuocDbContext dbc = new ChamSocMayLocNuocDbContext();
        private bool IsEdit = true;
        public int MaLoiLoc;
        public frmThemLoiLocMoi()
        {
            InitializeComponent();
            LoadCombobox();
            LoadLstLoiLoc();
        }

        public void SaveLoi()
        {
            DialogResult result = MessageBox.Show("Bạn có chắc muốn thêm mới lõi lọc \"" + txtTenLoi.Text + "\" không?", "Thông Báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (result.Equals(DialogResult.OK))
            {
                try
                {
                    foreach (var Item in chkcbLoaiMay.Properties.GetItems().Where(a => a.CheckState == CheckState.Checked))
                    {
                        LoiLoc loiLoc = new LoiLoc();
                        loiLoc.TenLoiLoc = txtTenLoi.Text;
                        loiLoc.ThoiGianThayLoi = int.Parse(cbTGThayLoi.EditValue.ToString());
                        loiLoc.MaLoaiMayLoc = int.Parse(Item.Value.ToString());
                        loiLoc.GhiChu = txtghiChu.Text;
                        dbc.LoiLocs.Add(loiLoc);
                    }
                    dbc.SaveChanges();
                    MessageBox.Show("Thêm mới thành công");
                }
                catch
                {
                    MessageBox.Show("Thêm mới thất bại");

                }
               
            }
           
            
        }
        public void CapNhatLoiLoc()
        {
            DialogResult result = MessageBox.Show("Bạn có chắc muốn cập nhật lõi lọc \"" + txtTenLoi.Text + "\" không?", "Thông Báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (result.Equals(DialogResult.OK))
            {

                    try
                    {
                        foreach (var Item in chkcbLoaiMay.Properties.GetItems().Where(a => a.CheckState == CheckState.Checked))
                        {
                            LoiLoc loiLoc = new LoiLoc();
                            loiLoc = dbc.LoiLocs.SingleOrDefault(s => s.MaLoiLoc == MaLoiLoc);
                            loiLoc.TenLoiLoc = txtTenLoi.Text;
                            loiLoc.ThoiGianThayLoi = int.Parse(cbTGThayLoi.EditValue.ToString());
                            loiLoc.MaLoaiMayLoc = int.Parse(Item.Value.ToString());
                            loiLoc.GhiChu = txtghiChu.Text;

                            dbc.SaveChanges();

                        }

                        MessageBox.Show("Cập nhật thành công");
                    }
                    catch
                    {
                        MessageBox.Show("Cập nhật thất bại");


                    }
            }

        }
        public void LoadCombobox()
        {

            cbTGThayLoi.Properties.DataSource = ComboBoxThoiGian.ListThoiGianBaoHanh();
            chkcbLoaiMay.Properties.DataSource = dbc.LoaiMayLocNuocs.ToList();
            grThayGianThayThe.DataSource = ComboBoxThoiGian.ListThoiGianBaoHanh();

        }

     

        private void btnSave_Click(object sender, EventArgs e)
        {
            // Check selected item
            if (txtTenLoi.Text != "")
            {
                if (!IsEdit)
                {
                    SaveLoi();
                }
                else
                {
                    CapNhatLoiLoc();
                }
                LoadLstLoiLoc();
                CleanForm();

            }
            else
            {
                MessageBox.Show("Bạn chưa chọn lõi lọc muốn cập nhật hoặc chưa nhập tên lõi lọc mới!");
                txtTenLoi.Focus();
            }
            LoadLstLoiLoc();
        }

        private void btnThemMoi_Click(object sender, EventArgs e)
        {
            IsEdit = false;
        }

        private void gvLMNL_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            IsEdit = true;
            LoiLoc loiLoc = new LoiLoc();
            loiLoc = gvLMNL.GetRow(e.RowHandle) as LoiLoc;
            if (loiLoc == null) return;
            MaLoiLoc = loiLoc.MaLoiLoc;
            txtTenLoi.Text = loiLoc.TenLoiLoc;
            cbTGThayLoi.EditValue = loiLoc.ThoiGianThayLoi;
            chkcbLoaiMay.SetEditValue(new object[] { loiLoc.MaLoaiMayLoc }) ;
            txtghiChu.Text = loiLoc.GhiChu;
        }
        public void LoadLstLoiLoc()
        {
            
            grdLMNL.DataSource = dbc.LoiLocs.ToList();
         
        }
        private void CleanForm()
        {
            txtTenLoi.Text = "";
            txtghiChu.Text = "";
            chkcbLoaiMay.SetEditValue(new object[] { });
            cbTGThayLoi.EditValue = null;
            cbThemMoiLoiLoc.Checked = false;
            // LoadLstLoiLoc();
            LoadCombobox();
        }

        private void LbGhiChu_Click(object sender, EventArgs e)
        {

        }

        private void cbThemMoiLoiLoc_CheckedChanged(object sender, EventArgs e)
        {
            if (cbThemMoiLoiLoc.Checked)
            {
                IsEdit = false;
            }
            else
            {
                IsEdit = true;
            }
        }

    }
}