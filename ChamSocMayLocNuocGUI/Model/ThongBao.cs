﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChamSocMayLocNuoc.Model
{
   public class ThongBao
    {
        public string MaKhachHang { get; set; }
        public string TenKhachHang { get; set; }
        public string DiaChi { get; set; }
        public string SoDienThoai { get; set; }
        public DateTime NgayMua { get; set;}
        public string ThoiGianThayLoi { get; set;}
        public int MaLichSuThayLoi { get; set; }
        public bool DaXem { get; set;}
        public bool DaGoi { get; set; }

    }
}
