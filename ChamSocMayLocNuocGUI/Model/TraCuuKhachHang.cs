﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChamSocMayLocNuoc.Model
{
    public class TraCuuKhachHang
    {
        public string MaKH { get; set; }
        public string TenKH { get; set; }
        public string SDT { get; set; }
        public string DiaChi { get; set; }
        public int MaLoaiMay { get; set; }
        public string TenLoaiMay { get; set; }
        public string TenLoiLoc { get; set; }
        public int MaLoiLoc { get; set; }
        public DateTime NgayMua { get; set; }
        public string ThoiGianThayThe { get; set; }
		public string GhiChu { get; set; }
        
    }
}
