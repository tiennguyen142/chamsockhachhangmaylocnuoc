﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChamSocMayLocNuoc.Model
{
    public class ComboBoxThoiGian
    {
        public ComboBoxThoiGian()
        {
            
        }
        public int Code {get;set;}
        public string Name { get; set; }

        public static List<ComboBoxThoiGian> ListThoiGianBaoHanh()
        {
            List<ComboBoxThoiGian> LstItem = new List<ComboBoxThoiGian>();
            
            ComboBoxThoiGian item_1 = new ComboBoxThoiGian();
            item_1.Code = 3;
            item_1.Name = "3 Tháng";
            LstItem.Add(item_1);
            ComboBoxThoiGian item_2 = new ComboBoxThoiGian();
            item_2.Code = 6;
            item_2.Name = "6 Tháng";
            LstItem.Add(item_2);
            ComboBoxThoiGian item_3 = new ComboBoxThoiGian();
            item_3.Code = 9;
            item_3.Name = "9 Tháng";
            LstItem.Add(item_3);
            ComboBoxThoiGian item_4 = new ComboBoxThoiGian();
            item_4.Code = 24;
            item_4.Name = "24 Tháng";
            LstItem.Add(item_4);
            ComboBoxThoiGian item_5 = new ComboBoxThoiGian();
            item_5.Code = 36;
            item_5.Name = "36 Tháng";
            LstItem.Add(item_5);
            ComboBoxThoiGian item_6 = new ComboBoxThoiGian();
            item_6.Code = 12;
            item_6.Name = "12 Tháng";
            LstItem.Add(item_6);
            ComboBoxThoiGian item_7 = new ComboBoxThoiGian();
            item_7.Code = 18;
            item_7.Name = "18 Tháng";
            LstItem.Add(item_7);

            return LstItem;
        }
    }
    
}
