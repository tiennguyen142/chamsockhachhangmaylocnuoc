﻿namespace ChamSocMayLocNuoc
{
    partial class frmLichSuNhacNho
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.label1 = new System.Windows.Forms.Label();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.grdLSNNho = new DevExpress.XtraGrid.GridControl();
            this.GvLSNNho = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcolMaKH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolTenKH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolSDT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolSP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolNMua = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolTHBHCLai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grdColNDTBao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.respbtnLSBH = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.respbtnSua = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdLSNNho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GvLSNNho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.respbtnLSBH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.respbtnSua)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.label1);
            this.panelControl2.Location = new System.Drawing.Point(1, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(873, 44);
            this.panelControl2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(189, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(447, 40);
            this.label1.TabIndex = 0;
            this.label1.Text = "Lịch Sử Nhắc Nhở";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.grdLSNNho);
            this.panelControl1.Location = new System.Drawing.Point(1, 45);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(872, 379);
            this.panelControl1.TabIndex = 2;
            // 
            // grdLSNNho
            // 
            this.grdLSNNho.Location = new System.Drawing.Point(5, 5);
            this.grdLSNNho.MainView = this.GvLSNNho;
            this.grdLSNNho.Name = "grdLSNNho";
            this.grdLSNNho.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.respbtnSua,
            this.respbtnLSBH});
            this.grdLSNNho.Size = new System.Drawing.Size(867, 369);
            this.grdLSNNho.TabIndex = 1;
            this.grdLSNNho.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GvLSNNho,
            this.gridView1});
            // 
            // GvLSNNho
            // 
            this.GvLSNNho.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcolMaKH,
            this.gcolTenKH,
            this.gcolSDT,
            this.gcolSP,
            this.gcolNMua,
            this.gcolTHBHCLai,
            this.grdColNDTBao});
            this.GvLSNNho.GridControl = this.grdLSNNho;
            this.GvLSNNho.Name = "GvLSNNho";
            this.GvLSNNho.OptionsView.ShowGroupPanel = false;
            // 
            // gcolMaKH
            // 
            this.gcolMaKH.Caption = "Mã Khách Hàng";
            this.gcolMaKH.MinWidth = 10;
            this.gcolMaKH.Name = "gcolMaKH";
            this.gcolMaKH.Visible = true;
            this.gcolMaKH.VisibleIndex = 0;
            this.gcolMaKH.Width = 106;
            // 
            // gcolTenKH
            // 
            this.gcolTenKH.Caption = "Tên Khách Hàng";
            this.gcolTenKH.FieldName = "gcolTenKH";
            this.gcolTenKH.MinWidth = 40;
            this.gcolTenKH.Name = "gcolTenKH";
            this.gcolTenKH.Visible = true;
            this.gcolTenKH.VisibleIndex = 1;
            this.gcolTenKH.Width = 145;
            // 
            // gcolSDT
            // 
            this.gcolSDT.Caption = "Số Điện Thoại";
            this.gcolSDT.MinWidth = 10;
            this.gcolSDT.Name = "gcolSDT";
            this.gcolSDT.Visible = true;
            this.gcolSDT.VisibleIndex = 2;
            this.gcolSDT.Width = 99;
            // 
            // gcolSP
            // 
            this.gcolSP.Caption = "Sản Phẩm";
            this.gcolSP.Name = "gcolSP";
            this.gcolSP.Visible = true;
            this.gcolSP.VisibleIndex = 3;
            // 
            // gcolNMua
            // 
            this.gcolNMua.Caption = "Ngày Mua";
            this.gcolNMua.MinWidth = 10;
            this.gcolNMua.Name = "gcolNMua";
            this.gcolNMua.Visible = true;
            this.gcolNMua.VisibleIndex = 4;
            this.gcolNMua.Width = 99;
            // 
            // gcolTHBHCLai
            // 
            this.gcolTHBHCLai.Caption = "Thời Hạn Bảo Hành Còn Lại";
            this.gcolTHBHCLai.MinWidth = 10;
            this.gcolTHBHCLai.Name = "gcolTHBHCLai";
            this.gcolTHBHCLai.Visible = true;
            this.gcolTHBHCLai.VisibleIndex = 5;
            this.gcolTHBHCLai.Width = 99;
            // 
            // grdColNDTBao
            // 
            this.grdColNDTBao.Caption = "Nội Dung Thông Báo";
            this.grdColNDTBao.ColumnEdit = this.respbtnLSBH;
            this.grdColNDTBao.Name = "grdColNDTBao";
            this.grdColNDTBao.Visible = true;
            this.grdColNDTBao.VisibleIndex = 6;
            this.grdColNDTBao.Width = 103;
            // 
            // respbtnLSBH
            // 
            this.respbtnLSBH.AutoHeight = false;
            this.respbtnLSBH.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.respbtnLSBH.Name = "respbtnLSBH";
            this.respbtnLSBH.NullValuePromptShowForEmptyValue = true;
            // 
            // respbtnSua
            // 
            this.respbtnSua.AutoHeight = false;
            this.respbtnSua.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.respbtnSua.Name = "respbtnSua";
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8});
            this.gridView1.GridControl = this.grdLSNNho;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Mã Khách Hàng";
            this.gridColumn1.MinWidth = 10;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Width = 106;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Tên Khách Hàng";
            this.gridColumn2.FieldName = "gcolTenKH";
            this.gridColumn2.MinWidth = 40;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Width = 145;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Số Điện Thoại";
            this.gridColumn3.MinWidth = 10;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Width = 99;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Sản Phẩm";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Width = 99;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Ngày Mua";
            this.gridColumn5.MinWidth = 10;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Width = 99;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Thời Hạn Bảo Hành";
            this.gridColumn6.MinWidth = 10;
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Width = 99;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Lịch Sử Bảo Hành";
            this.gridColumn7.ColumnEdit = this.respbtnLSBH;
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Width = 103;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Chức Năng";
            this.gridColumn8.ColumnEdit = this.respbtnSua;
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Width = 99;
            // 
            // LichSuNhacNho
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 425);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelControl2);
            this.Name = "LichSuNhacNho";
            this.ShowIcon = false;
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdLSNNho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GvLSNNho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.respbtnLSBH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.respbtnSua)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl grdLSNNho;
        private DevExpress.XtraGrid.Views.Grid.GridView GvLSNNho;
        private DevExpress.XtraGrid.Columns.GridColumn gcolMaKH;
        private DevExpress.XtraGrid.Columns.GridColumn gcolTenKH;
        private DevExpress.XtraGrid.Columns.GridColumn gcolSDT;
        private DevExpress.XtraGrid.Columns.GridColumn gcolSP;
        private DevExpress.XtraGrid.Columns.GridColumn gcolNMua;
        private DevExpress.XtraGrid.Columns.GridColumn gcolTHBHCLai;
        private DevExpress.XtraGrid.Columns.GridColumn grdColNDTBao;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit respbtnLSBH;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit respbtnSua;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
    }
}