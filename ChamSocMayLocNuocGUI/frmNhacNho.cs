﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ChamSocMayLocNuoc.Model;
using ChamSocMayLocNuocDAL.Model;
using System.Data.Entity.SqlServer;
using DevExpress.XtraGrid.Views.Grid;

namespace ChamSocMayLocNuoc
{
    public partial class frmNhacNho : DevExpress.XtraEditors.XtraForm
    {
        ChamSocMayLocNuocDbContext dbc = new ChamSocMayLocNuocDbContext();
        public frmNhacNho()
        {
            InitializeComponent();
            gvThongBao.IndicatorWidth = 45;
            LoadData();
        }
        public void LoadData()
        {
            grThoiGianThayLoi.DataSource = ComboBoxThoiGian.ListThoiGianBaoHanh();

            var query = (from rg in dbc.KhachHangs
                         join s in dbc.LichSuThayLoiLocs on rg.MaKH equals s.MaKH
                         join a in dbc.LoiLocs on s.MaLoiLoc equals a.MaLoiLoc
                         // where SqlFunctions.DateAdd("month", a.ThoiGianThayLoi, s.NgayMua) >= SqlFunctions.DateAdd("month", -1, DateTime.Today.Date)
                         where SqlFunctions.DateAdd("month", a.ThoiGianThayLoi, s.NgayMua) <= DateTime.Today.Date
                         where s.DaGoi == false
                         select new ThongBao()
                         {
                             MaKhachHang = rg.MaKH,
                             TenKhachHang = rg.TenKH,
                             SoDienThoai = rg.SDT,
                             DiaChi = rg.DiaChi,
                             NgayMua = s.NgayMua,
                             ThoiGianThayLoi = a.ThoiGianThayLoi.ToString(),
                             DaGoi = s.DaGoi,
                             DaXem=s.DaXem,
                             MaLichSuThayLoi = s.MaLS
                             
            
                         });

                grdThongBao.DataSource = query.ToList();
           
        }

        private void gvThongBao_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.Name == "girdDagoi" && MessageBox.Show("Bạn có muốn chọn Đã Thay Lõi Không?", "Thông Báo", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                LoadData();
                return;
            }
            if (e.Column.Name == "girdDagoi" || e.Column.Name== "girdDaxem")
            {
                ThongBao TB = new ThongBao();
                TB = gvThongBao.GetRow(e.RowHandle) as ThongBao;
                Update(TB.DaGoi, TB.DaXem, TB.MaLichSuThayLoi);
                LoadData();
            }
           
        }
        
        private void Update(bool DaGoi,bool DaXem, int MaLsThayLois)
        {
            LichSuThayLoiLoc lsll = new LichSuThayLoiLoc();
            lsll = dbc.LichSuThayLoiLocs.SingleOrDefault(s => s.MaLS == MaLsThayLois);
            lsll.DaXem = DaXem;
            lsll.DaGoi = DaGoi;
            dbc.SaveChanges();

        }
        private void gvThongBao_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            //GridView view = sender as GridView;
            ////if (view.FocusedRowHandle == e.RowHandle && !view.FocusedColumn.Equals(e.Column))
            ////    e.Appearance.BackColor = Color.Orange;
            //if (e.RowHandle < 0)
            //    return;
            //ThongBao tb = view.GetRow(e.RowHandle) as ThongBao;
            //if (e.Column.Name == "girdDagoi" && tb.DaGoi)//&& e.CellValue.Equals(1))
            //{
            //    e.Appearance.BackColor = Color.Red;
            //}
            //if (e.Column.Name == "girdDaxem" && tb.DaXem)//&& e.CellValue.Equals(1))
            //{
            //    e.Appearance.BackColor = Color.Yellow;
            //}
            if (e.RowHandle < 0)
                return;
            GridView view = sender as GridView;
            ThongBao tb = view.GetRow(e.RowHandle) as ThongBao;
            if (!tb.DaGoi)
            {
                e.Appearance.BackColor = Color.LightGreen;
            }
            if (!tb.DaXem)
            {
                e.Appearance.BackColor = Color.LightYellow;
            }
            if (!tb.DaXem && !tb.DaGoi)
            {
                e.Appearance.BackColor = Color.OrangeRed;
            }
        }

        private void gvThongBao_RowStyle(object sender, RowStyleEventArgs e)
        {

            if (e.RowHandle < 0)
                return;
            GridView view = sender as GridView;
            ThongBao tb = view.GetRow(e.RowHandle) as ThongBao;
            if (!tb.DaGoi)
            {
                e.Appearance.BackColor = Color.LightGreen;
            }
            if (!tb.DaXem)
            {
                e.Appearance.BackColor = Color.LightYellow;
            }
            if (!tb.DaXem && !tb.DaGoi)
            {
                e.Appearance.BackColor = Color.OrangeRed;
            }
        }

        private void gvThongBao_CustomDrawRowIndicator(object sender, RowIndicatorCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
                e.Info.DisplayText = e.RowHandle.ToString();
        }
    }
}