﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ChamSocMayLocNuocDAL.Model;
using DevExpress.XtraEditors.Controls;
using ChamSocMayLocNuoc.Model;
using System.Globalization;

namespace ChamSocMayLocNuoc
{
    
    public partial class frmThemKhachHang : DevExpress.XtraEditors.XtraForm
    {
        public static bool IsEdit =true;
        List<LoiLoc> LoiLocLst = new List<LoiLoc>();
        List<LoaiMayLocNuoc> LoaiMayNuocLocLst = new List<LoaiMayLocNuoc>();
        ChamSocMayLocNuocDbContext dbc = new ChamSocMayLocNuocDbContext();
        int MaLoaiMay;
        int MaLoaiLoi;
        public frmThemKhachHang()
        {
            InitializeComponent();
            GvTCKH.IndicatorWidth = 45;
            LoadCombobox();
            LoadKh();
            
        }

        public void AddKhachHang(string maKh)
        {
            KhachHang Kh = new KhachHang();
            Kh.MaKH = maKh;
            Kh.TenKH = txtTenKH.Text;
            Kh.SDT = txtSDT.Text;
            Kh.DiaChi = txtDiaChi.Text;
            dbc.KhachHangs.Add(Kh);
            dbc.SaveChanges();

        }
        public void UpdateKhachHang()
        {
            KhachHang Kh = new KhachHang();
            Kh = dbc.KhachHangs.SingleOrDefault(x=>x.MaKH == txtMaKh.Text) ;
            Kh.TenKH = txtTenKH.Text;
            Kh.SDT = txtSDT.Text;
            Kh.DiaChi = txtDiaChi.Text;
            
            dbc.SaveChanges();

        }
        public void AddLichSuMuaMay(string MaKH)
        {
            foreach (var Item in chkcbLoaiMay.Properties.GetItems().Where(a => a.CheckState == CheckState.Checked))
            {
                 
                LichSuMuaMay lsMuaMay = new LichSuMuaMay();
                lsMuaMay.MaKH = MaKH;
                lsMuaMay.MaLoaiMay = int.Parse(Item.Value.ToString());
                lsMuaMay.GhiChu = txtGhiChu.Text;
                lsMuaMay.SoLuong = 1;
                lsMuaMay.NgayMua = DateTime.ParseExact(string.IsNullOrEmpty(cldNgayMua.Text)?"00-00-0000": cldNgayMua.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                dbc.LichSuMuaMays.Add(lsMuaMay);
               
            }
            dbc.SaveChanges();
        }

        public void RemoveLichSuMuaMay(string MaKH)
        {
            foreach (var Item in dbc.LichSuMuaMays.Where(s => s.MaKH == MaKH && s.MaLoaiMay == MaLoaiMay))
            {
                dbc.LichSuMuaMays.Remove(Item);
            }
            dbc.SaveChanges();
        }
        public void LoadKh()
        {

            var query = from rg in dbc.KhachHangs
                        join srg in dbc.LichSuMuaMays on rg.MaKH equals srg.MaKH
                        join s in dbc.LichSuThayLoiLocs on srg.MaKH equals s.MaKH
                        join a in dbc.LoiLocs on s.MaLoiLoc equals a.MaLoiLoc
                        join e in dbc.LoaiMayLocNuocs on srg.MaLoaiMay equals e.MaLoaiMay

                        select new TraCuuKhachHang()
                        {
                            MaKH = rg.MaKH,
                            TenKH = rg.TenKH,
                            SDT = rg.SDT,
                            MaLoiLoc = a.MaLoiLoc,
                            MaLoaiMay = e.MaLoaiMay,
                            TenLoaiMay = e.TenLoaiMay,
                            TenLoiLoc = a.TenLoiLoc,
                            NgayMua = s.NgayMua,
                            DiaChi = rg.DiaChi,
                            ThoiGianThayThe = a.ThoiGianThayLoi.ToString(),
                            GhiChu = srg.GhiChu

                        };
            grdTCKH.DataSource = query.ToList();
        }

        public void AddLichSuMuaLoi(string MaKH)
        {
            foreach (var Item in chkcbLoaiLoiLoc.Properties.GetItems().Where(a => a.CheckState == CheckState.Checked))
            {
                LichSuThayLoiLoc lsMuaLoi = new LichSuThayLoiLoc();
                lsMuaLoi.MaKH = MaKH;
                lsMuaLoi.SoLuong = 1;
                lsMuaLoi.MaLoiLoc = int.Parse(Item.Value.ToString());
                lsMuaLoi.GhiChu = txtDiaChi.Text;
                lsMuaLoi.NgayMua = DateTime.ParseExact(string.IsNullOrEmpty(cldNgayMua.Text) ? "00-00-0000" : cldNgayMua.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                dbc.LichSuThayLoiLocs.Add(lsMuaLoi);
            }
            dbc.SaveChanges();
        }
        public void RemoveLichSuMuaLoi(string MaKH)
        {
            foreach (var Item in dbc.LichSuThayLoiLocs.Where(s=>s.MaKH==MaKH && s.MaLoiLoc==MaLoaiLoi))
            {
                dbc.LichSuThayLoiLocs.Remove(Item);
            }
            dbc.SaveChanges();
        }
        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (txtTenKH.Text != "")
            {
                    try
                    {
                        if (!IsEdit)
                        {
                            DialogResult result = MessageBox.Show("Bạn có chắc muốn thêm khách hàng \"" + txtTenKH.Text + "\" không?", "Thông Báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                            if (result.Equals(DialogResult.OK))
                            {
                                string generateMaKh = generateID();
                                AddKhachHang(generateMaKh);
                                AddLichSuMuaMay(generateMaKh);
                                AddLichSuMuaLoi(generateMaKh);
                                MessageBox.Show("Thêm khách hàng thành công!");
                                CleanForm();
                                LoadKh();
                            }
                        }
                        else
                        {
                            DialogResult result = MessageBox.Show("Bạn có chắc muốn cập nhật khách hàng \"" + txtTenKH.Text + "\" không?", "Thông Báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                            if (result.Equals(DialogResult.OK))
                            {
                                UpdateKhachHang();
                                RemoveLichSuMuaMay(txtMaKh.Text);
                                RemoveLichSuMuaLoi(txtMaKh.Text);
                                AddLichSuMuaMay(txtMaKh.Text);
                                AddLichSuMuaLoi(txtMaKh.Text);
                                MessageBox.Show("Cập Nhật khách hàng thành công!");
                                CleanForm();
                                LoadKh();
                            }

                        }


                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Thêm/Cập nhật khách hàng thất bại!", ex.Message);
                    }
       
                

            }
            else
            {
                MessageBox.Show("Bạn chưa chọn khách hàng muốn cập nhật hoặc chưa nhập tên khách hàng mới!");
                txtTenKH.Focus();
            }

        }
        public string generateID()
        {
            long i = 1;

            foreach (byte b in Guid.NewGuid().ToByteArray())
            {
                i *= ((int)b + 1);
            }

            string number = String.Format("{0:d9}", (DateTime.Now.Ticks / 10) % 1000000000);

            return number;
        }

        private void chkcbLoaiMay_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                List<LoiLoc> tempLoiLoc = new List<LoiLoc>();
                foreach (var Item in chkcbLoaiMay.Properties.GetItems().Where(a => a.CheckState == CheckState.Checked))
                {
                    tempLoiLoc.AddRange(LoiLocLst.Where(a => a.MaLoaiMayLoc == int.Parse(Item.Value.ToString())));
                }
                chkcbLoaiLoiLoc.Properties.DataSource = tempLoiLoc;
                chkcbLoaiLoiLoc.Refresh();
            }
            catch
            {

            }
            
        }
        public void LoadCombobox()
        {
            
            chkcbLoaiMay.Properties.DataSource = dbc.LoaiMayLocNuocs.ToList();
            chkcbLoaiLoiLoc.Properties.DataSource = LoiLocLst = dbc.LoiLocs.ToList();
            grThayGianThayThe.DataSource = ComboBoxThoiGian.ListThoiGianBaoHanh();
            //List<LoiLoc> LoiLoclst = new List<LoiLoc>();
            //foreach (var item in dbc.LoiLocs.ToList())
            //{
            //    item.TenLoiLoc = item.TenLoiLoc + " ( " + item.ThoiGianThayLoi + " Tháng )";
            //    LoiLoclst.Add(item);
            //}

           // chkcbLoaiLoiLoc.Properties.DataSource = LoiLoclst;
            chkcbLoaiLoiLoc.Refresh();
        }

        private void GvTCKH_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            IsEdit = true;
            TraCuuKhachHang KhachHang = new TraCuuKhachHang();
            KhachHang = GvTCKH.GetRow(e.RowHandle) as TraCuuKhachHang;
            if (KhachHang == null) return;
          
            chkcbLoaiMay.SetEditValue(new object[] { KhachHang.MaLoaiMay });
            chkcbLoaiLoiLoc.SetEditValue(new object[] { KhachHang.MaLoiLoc });
            txtDiaChi.Text = KhachHang.DiaChi;
            txtMaKh.Text = KhachHang.MaKH;
            txtTenKH.Text = KhachHang.TenKH;
            cldNgayMua.EditValue = KhachHang.NgayMua;
            txtSDT.Text = KhachHang.SDT;
            MaLoaiMay = KhachHang.MaLoaiMay;
            MaLoaiLoi = KhachHang.MaLoiLoc;
            txtGhiChu.Text = KhachHang.GhiChu;

        }

        private void CleanForm()
        {
            txtMaKh.Text = "";
            txtDiaChi.Text = "";
            chkcbLoaiMay.SetEditValue(new object[] { });
            chkcbLoaiLoiLoc.SetEditValue(new object[] { });
            txtTenKH.Text = "";
            cldNgayMua.EditValue = "";
            txtSDT.Text = "";
            txtGhiChu.Text = "";
            cbThemKhachHang.Checked = false;
        }


        private void btnHuy_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cbThemKhachHang_CheckedChanged(object sender, EventArgs e)
        {
            if (cbThemKhachHang.Checked)
            {
                IsEdit = false;
            }
            else
            {
                IsEdit = true;
            }
        }

        private void btnXoaThongTin_Click(object sender, EventArgs e)
        {
            CleanForm();
            IsEdit = true;
        }

        private void GvTCKH_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
                e.Info.DisplayText = e.RowHandle.ToString();
        }
    }
}