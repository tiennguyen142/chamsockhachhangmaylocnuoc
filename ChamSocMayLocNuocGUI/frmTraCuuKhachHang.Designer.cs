﻿namespace ChamSocMayLocNuoc
{
    partial class frmTraCuuKhachHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbGhiChu = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbTenKHang = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbLoaiLoiLoc = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbSoDienThoai = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbTenLoaiMay = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.grdTCKH = new DevExpress.XtraGrid.GridControl();
            this.GvTCKH = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcolMaKH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolTenKH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolSDT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolSP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvColTenLoi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolNMua = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gcolTHBH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grThayGianThayThe = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.btnThayTheLoi = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdTCKH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GvTCKH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grThayGianThayThe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.groupBox1);
            this.panelControl1.Location = new System.Drawing.Point(0, 45);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1183, 514);
            this.panelControl1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbGhiChu);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.lbTenKHang);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.lbLoaiLoiLoc);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.lbSoDienThoai);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lbTenLoaiMay);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(12, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1171, 143);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông Tin Khách Hàng";
            // 
            // lbGhiChu
            // 
            this.lbGhiChu.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGhiChu.Location = new System.Drawing.Point(156, 92);
            this.lbGhiChu.Name = "lbGhiChu";
            this.lbGhiChu.Size = new System.Drawing.Size(671, 27);
            this.lbGhiChu.TabIndex = 2;
            this.lbGhiChu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(76, 92);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 27);
            this.label6.TabIndex = 1;
            this.label6.Text = "Ghi chú:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbTenKHang
            // 
            this.lbTenKHang.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTenKHang.Location = new System.Drawing.Point(156, 26);
            this.lbTenKHang.Name = "lbTenKHang";
            this.lbTenKHang.Size = new System.Drawing.Size(233, 24);
            this.lbTenKHang.TabIndex = 0;
            this.lbTenKHang.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(10, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 24);
            this.label3.TabIndex = 0;
            this.label3.Text = "Tên Khách Hàng :";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbLoaiLoiLoc
            // 
            this.lbLoaiLoiLoc.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLoaiLoiLoc.Location = new System.Drawing.Point(482, 52);
            this.lbLoaiLoiLoc.Name = "lbLoaiLoiLoc";
            this.lbLoaiLoiLoc.Size = new System.Drawing.Size(345, 22);
            this.lbLoaiLoiLoc.TabIndex = 0;
            this.lbLoaiLoiLoc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(395, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 22);
            this.label5.TabIndex = 0;
            this.label5.Text = "Loại Lõi :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbSoDienThoai
            // 
            this.lbSoDienThoai.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSoDienThoai.Location = new System.Drawing.Point(156, 54);
            this.lbSoDienThoai.Name = "lbSoDienThoai";
            this.lbSoDienThoai.Size = new System.Drawing.Size(233, 27);
            this.lbSoDienThoai.TabIndex = 0;
            this.lbSoDienThoai.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(25, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 27);
            this.label2.TabIndex = 0;
            this.label2.Text = "Số Điện Thoại :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbTenLoaiMay
            // 
            this.lbTenLoaiMay.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTenLoaiMay.Location = new System.Drawing.Point(478, 26);
            this.lbTenLoaiMay.Name = "lbTenLoaiMay";
            this.lbTenLoaiMay.Size = new System.Drawing.Size(349, 22);
            this.lbTenLoaiMay.TabIndex = 0;
            this.lbTenLoaiMay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(395, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 22);
            this.label4.TabIndex = 0;
            this.label4.Text = "Loại Máy :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // grdTCKH
            // 
            this.grdTCKH.Location = new System.Drawing.Point(12, 196);
            this.grdTCKH.MainView = this.GvTCKH;
            this.grdTCKH.Name = "grdTCKH";
            this.grdTCKH.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.grThayGianThayThe,
            this.repositoryItemDateEdit1});
            this.grdTCKH.Size = new System.Drawing.Size(1171, 363);
            this.grdTCKH.TabIndex = 1;
            this.grdTCKH.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GvTCKH});
            // 
            // GvTCKH
            // 
            this.GvTCKH.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcolMaKH,
            this.gcolTenKH,
            this.gcolSDT,
            this.gcolSP,
            this.gvColTenLoi,
            this.gcolNMua,
            this.gcolTHBH});
            this.GvTCKH.GridControl = this.grdTCKH;
            this.GvTCKH.Name = "GvTCKH";
            this.GvTCKH.OptionsBehavior.Editable = false;
            this.GvTCKH.OptionsBehavior.ReadOnly = true;
            this.GvTCKH.OptionsView.ShowAutoFilterRow = true;
            this.GvTCKH.OptionsView.ShowGroupPanel = false;
            this.GvTCKH.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.GvTCKH_RowClick);
            this.GvTCKH.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.GvTCKH_CustomDrawRowIndicator);
            // 
            // gcolMaKH
            // 
            this.gcolMaKH.Caption = "Mã Khách Hàng";
            this.gcolMaKH.FieldName = "MaKH";
            this.gcolMaKH.MinWidth = 10;
            this.gcolMaKH.Name = "gcolMaKH";
            this.gcolMaKH.OptionsColumn.AllowEdit = false;
            this.gcolMaKH.OptionsFilter.AllowAutoFilter = false;
            this.gcolMaKH.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gcolMaKH.OptionsFilter.ImmediateUpdateAutoFilter = false;
            this.gcolMaKH.OptionsFilter.ImmediateUpdatePopupDateFilterOnCheck = DevExpress.Utils.DefaultBoolean.False;
            this.gcolMaKH.OptionsFilter.ImmediateUpdatePopupDateFilterOnDateChange = DevExpress.Utils.DefaultBoolean.False;
            this.gcolMaKH.OptionsFilter.ImmediateUpdatePopupExcelFilter = DevExpress.Utils.DefaultBoolean.False;
            this.gcolMaKH.Visible = true;
            this.gcolMaKH.VisibleIndex = 0;
            this.gcolMaKH.Width = 86;
            // 
            // gcolTenKH
            // 
            this.gcolTenKH.Caption = "Tên Khách Hàng";
            this.gcolTenKH.FieldName = "TenKH";
            this.gcolTenKH.MinWidth = 40;
            this.gcolTenKH.Name = "gcolTenKH";
            this.gcolTenKH.Visible = true;
            this.gcolTenKH.VisibleIndex = 1;
            this.gcolTenKH.Width = 178;
            // 
            // gcolSDT
            // 
            this.gcolSDT.Caption = "Số Điện Thoại";
            this.gcolSDT.FieldName = "SDT";
            this.gcolSDT.MinWidth = 10;
            this.gcolSDT.Name = "gcolSDT";
            this.gcolSDT.Visible = true;
            this.gcolSDT.VisibleIndex = 2;
            this.gcolSDT.Width = 152;
            // 
            // gcolSP
            // 
            this.gcolSP.Caption = "Tên Loại Máy";
            this.gcolSP.FieldName = "TenLoaiMay";
            this.gcolSP.Name = "gcolSP";
            this.gcolSP.Visible = true;
            this.gcolSP.VisibleIndex = 3;
            this.gcolSP.Width = 123;
            // 
            // gvColTenLoi
            // 
            this.gvColTenLoi.Caption = "Tên Lõi";
            this.gvColTenLoi.FieldName = "TenLoiLoc";
            this.gvColTenLoi.Name = "gvColTenLoi";
            this.gvColTenLoi.Visible = true;
            this.gvColTenLoi.VisibleIndex = 4;
            this.gvColTenLoi.Width = 112;
            // 
            // gcolNMua
            // 
            this.gcolNMua.Caption = "Ngày Mua";
            this.gcolNMua.ColumnEdit = this.repositoryItemDateEdit1;
            this.gcolNMua.FieldName = "NgayMua";
            this.gcolNMua.MinWidth = 10;
            this.gcolNMua.Name = "gcolNMua";
            this.gcolNMua.Visible = true;
            this.gcolNMua.VisibleIndex = 5;
            this.gcolNMua.Width = 148;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // gcolTHBH
            // 
            this.gcolTHBH.Caption = "Thời Gian Thay Thế Lõi";
            this.gcolTHBH.ColumnEdit = this.grThayGianThayThe;
            this.gcolTHBH.FieldName = "ThoiGianThayThe";
            this.gcolTHBH.MinWidth = 10;
            this.gcolTHBH.Name = "gcolTHBH";
            this.gcolTHBH.Visible = true;
            this.gcolTHBH.VisibleIndex = 6;
            this.gcolTHBH.Width = 160;
            // 
            // grThayGianThayThe
            // 
            this.grThayGianThayThe.AutoHeight = false;
            this.grThayGianThayThe.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.grThayGianThayThe.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Thời gian thay lõi"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Mã", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default)});
            this.grThayGianThayThe.DisplayMember = "Name";
            this.grThayGianThayThe.KeyMember = "Code";
            this.grThayGianThayThe.Name = "grThayGianThayThe";
            this.grThayGianThayThe.NullText = "";
            this.grThayGianThayThe.ValueMember = "Code";
            // 
            // btnThayTheLoi
            // 
            this.btnThayTheLoi.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThayTheLoi.Appearance.Options.UseFont = true;
            this.btnThayTheLoi.Location = new System.Drawing.Point(867, 67);
            this.btnThayTheLoi.Name = "btnThayTheLoi";
            this.btnThayTheLoi.Size = new System.Drawing.Size(223, 61);
            this.btnThayTheLoi.TabIndex = 3;
            this.btnThayTheLoi.Text = "Thay Lõi Mới";
            this.btnThayTheLoi.Click += new System.EventHandler(this.btnThayTheLoi_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.label1);
            this.panelControl2.Location = new System.Drawing.Point(0, -3);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1183, 44);
            this.panelControl2.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1183, 38);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tra Cứu Khách Hàng";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmTraCuuKhachHang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 561);
            this.Controls.Add(this.grdTCKH);
            this.Controls.Add(this.btnThayTheLoi);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "frmTraCuuKhachHang";
            this.ShowIcon = false;
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdTCKH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GvTCKH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grThayGianThayThe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl grdTCKH;
        private DevExpress.XtraGrid.Views.Grid.GridView GvTCKH;
        private DevExpress.XtraGrid.Columns.GridColumn gcolMaKH;
        private DevExpress.XtraGrid.Columns.GridColumn gcolTenKH;
        private DevExpress.XtraGrid.Columns.GridColumn gcolSDT;
        private DevExpress.XtraGrid.Columns.GridColumn gcolSP;
        private DevExpress.XtraGrid.Columns.GridColumn gcolNMua;
        private DevExpress.XtraGrid.Columns.GridColumn gcolTHBH;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.SimpleButton btnThayTheLoi;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraGrid.Columns.GridColumn gvColTenLoi;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbTenKHang;
        private System.Windows.Forms.Label lbLoaiLoiLoc;
        private System.Windows.Forms.Label lbSoDienThoai;
        private System.Windows.Forms.Label lbTenLoaiMay;
        public DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit grThayGianThayThe;
        private System.Windows.Forms.Label lbGhiChu;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
    }
}