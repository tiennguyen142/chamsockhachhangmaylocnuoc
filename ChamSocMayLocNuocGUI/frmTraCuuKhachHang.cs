﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ChamSocMayLocNuocDAL.Model;
using ChamSocMayLocNuoc.Model;

namespace ChamSocMayLocNuoc
{
    public partial class frmTraCuuKhachHang : DevExpress.XtraEditors.XtraForm
    {
        ChamSocMayLocNuocDbContext dbc = new ChamSocMayLocNuocDbContext();
        public TraCuuKhachHang MaKh;
        public frmTraCuuKhachHang()
        {
            InitializeComponent();
            GvTCKH.IndicatorWidth = 45;
            LoadCombobox();
            LoadKh();
        }

        private void btnThayTheLoi_Click(object sender, EventArgs e)
        {
            if(MaKh!=null)
            {
                frmThemLichSuThayLoiLoc frmthayloi = new frmThemLichSuThayLoiLoc(MaKh);
                frmthayloi.Show();
            }
            else
            {
                MessageBox.Show("Bạn chưa chọn khách hàng muốn thay lõi!");
            }


        }
        public void LoadKh()
        {

            var query = from rg in dbc.KhachHangs
                        join srg in dbc.LichSuMuaMays on rg.MaKH equals srg.MaKH
                        join s in dbc.LichSuThayLoiLocs on srg.MaKH equals s.MaKH
                        join a in dbc.LoiLocs on s.MaLoiLoc equals a.MaLoiLoc
                        join e in dbc.LoaiMayLocNuocs on srg.MaLoaiMay equals e.MaLoaiMay

                        select new TraCuuKhachHang()
                        {
                            MaKH = rg.MaKH,
                            TenKH = rg.TenKH,
                            SDT = rg.SDT,
                            TenLoaiMay = e.TenLoaiMay,
                            TenLoiLoc = a.TenLoiLoc,
                            NgayMua =  s.NgayMua,
                            ThoiGianThayThe = a.ThoiGianThayLoi.ToString(),
                            GhiChu = srg.GhiChu

                        };
            grdTCKH.DataSource = query.ToList();
        }

        private void GvTCKH_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            try
            {
                TraCuuKhachHang Kh = new TraCuuKhachHang();
                Kh = GvTCKH.GetRow(e.RowHandle) as TraCuuKhachHang;
                lbLoaiLoiLoc.Text = Kh.TenLoiLoc;
                lbSoDienThoai.Text = Kh.SDT;
                lbTenKHang.Text = Kh.TenKH;
                lbTenLoaiMay.Text = Kh.TenLoaiMay;
                lbGhiChu.Text = Kh.GhiChu;
                MaKh = Kh;
            }
            catch
            {

            }

        }
        public void LoadCombobox()
        {


            //cbTGThayLoi.Properties.DataSource = ComboBoxThoiGian.ListThoiGianBaoHanh();
            //chkcbLoaiMay.Properties.DataSource = dbc.LoaiMayLocNuocs.ToList();
            grThayGianThayThe.DataSource = ComboBoxThoiGian.ListThoiGianBaoHanh();

        }

        private void GvTCKH_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
                e.Info.DisplayText = e.RowHandle.ToString();
        }
    }
}