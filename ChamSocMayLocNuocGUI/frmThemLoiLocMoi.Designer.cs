﻿namespace ChamSocMayLocNuoc
{
    partial class frmThemLoiLocMoi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmThemLoiLocMoi));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lb_name = new System.Windows.Forms.Label();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.grdLMNL = new DevExpress.XtraGrid.GridControl();
            this.gvLMNL = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gvcolMaLoiLoc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolTenLoiLoc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvcolThoiGianThayLoi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grThayGianThayThe = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gvGhiChu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtghiChu = new System.Windows.Forms.TextBox();
            this.chkcbLoaiMay = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.btnCapNhat = new DevExpress.XtraEditors.SimpleButton();
            this.txtTenLoi = new System.Windows.Forms.TextBox();
            this.LbGhiChu = new System.Windows.Forms.Label();
            this.lbLoaiLoi = new System.Windows.Forms.Label();
            this.lbTGTu = new System.Windows.Forms.Label();
            this.lbTenLoi = new System.Windows.Forms.Label();
            this.cbTGThayLoi = new DevExpress.XtraEditors.LookUpEdit();
			this.cbThemMoiLoiLoc = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdLMNL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLMNL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grThayGianThayThe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkcbLoaiMay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTGThayLoi.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Controls.Add(this.lb_name);
            this.panelControl1.Location = new System.Drawing.Point(0, 1);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(984, 41);
            this.panelControl1.TabIndex = 1;
            // 
            // lb_name
            // 
            this.lb_name.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_name.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_name.Location = new System.Drawing.Point(57, 2);
            this.lb_name.Name = "lb_name";
            this.lb_name.Size = new System.Drawing.Size(891, 34);
            this.lb_name.TabIndex = 0;
            this.lb_name.Text = "Lõi Máy Lọc Nước";
            this.lb_name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.panelControl2.Controls.Add(this.cbThemMoiLoiLoc);
            this.panelControl2.Controls.Add(this.grdLMNL);
            this.panelControl2.Controls.Add(this.txtghiChu);
            this.panelControl2.Controls.Add(this.chkcbLoaiMay);
            this.panelControl2.Controls.Add(this.btnCapNhat);
            this.panelControl2.Controls.Add(this.txtTenLoi);
            this.panelControl2.Controls.Add(this.LbGhiChu);
            this.panelControl2.Controls.Add(this.lbLoaiLoi);
            this.panelControl2.Controls.Add(this.lbTGTu);
            this.panelControl2.Controls.Add(this.lbTenLoi);
            this.panelControl2.Controls.Add(this.cbTGThayLoi);
            this.panelControl2.Location = new System.Drawing.Point(0, 40);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(984, 518);
            this.panelControl2.TabIndex = 2;
            // 
            // grdLMNL
            // 
            this.grdLMNL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.grdLMNL.Location = new System.Drawing.Point(0, 199);
            this.grdLMNL.MainView = this.gvLMNL;
            this.grdLMNL.Name = "grdLMNL";
            this.grdLMNL.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.grThayGianThayThe});
            this.grdLMNL.Size = new System.Drawing.Size(984, 284);
            this.grdLMNL.TabIndex = 29;
            this.grdLMNL.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvLMNL});
            // 
            // gvLMNL
            // 
            this.gvLMNL.ActiveFilterEnabled = false;
            this.gvLMNL.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gvLMNL.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvLMNL.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gvcolMaLoiLoc,
            this.gcolTenLoiLoc,
            this.gvcolThoiGianThayLoi,
            this.gvGhiChu});
            this.gvLMNL.GridControl = this.grdLMNL;
            this.gvLMNL.Name = "gvLMNL";
            this.gvLMNL.OptionsCustomization.AllowFilter = false;
            this.gvLMNL.OptionsFilter.AllowAutoFilterConditionChange = DevExpress.Utils.DefaultBoolean.True;
            this.gvLMNL.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gvLMNL.OptionsFilter.AllowFilterEditor = false;
            this.gvLMNL.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gvLMNL.OptionsFilter.AllowMRUFilterList = false;
            this.gvLMNL.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gvLMNL.OptionsFilter.ColumnFilterPopupMode = DevExpress.XtraGrid.Columns.ColumnFilterPopupMode.Classic;
            this.gvLMNL.OptionsFilter.DefaultFilterEditorView = DevExpress.XtraEditors.FilterEditorViewMode.Text;
            this.gvLMNL.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gvLMNL.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gvLMNL.OptionsFilter.ShowAllTableValuesInFilterPopup = true;
            this.gvLMNL.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gvLMNL.OptionsView.ShowAutoFilterRow = true;
            this.gvLMNL.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gvLMNL.OptionsView.ShowGroupPanel = false;
            this.gvLMNL.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gvLMNL_RowClick);
            // 
            // gvcolMaLoiLoc
            // 
            this.gvcolMaLoiLoc.Caption = "Mã Lõi Lọc";
            this.gvcolMaLoiLoc.FieldName = "MaLoiLoc";
            this.gvcolMaLoiLoc.Name = "gvcolMaLoiLoc";
            this.gvcolMaLoiLoc.OptionsColumn.AllowEdit = false;
            this.gvcolMaLoiLoc.Visible = true;
            this.gvcolMaLoiLoc.VisibleIndex = 0;
            this.gvcolMaLoiLoc.Width = 145;
            // 
            // gcolTenLoiLoc
            // 
            this.gcolTenLoiLoc.Caption = "Tên Lõi Lọc";
            this.gcolTenLoiLoc.FieldName = "TenLoiLoc";
            this.gcolTenLoiLoc.Name = "gcolTenLoiLoc";
            this.gcolTenLoiLoc.OptionsColumn.AllowEdit = false;
            this.gcolTenLoiLoc.Visible = true;
            this.gcolTenLoiLoc.VisibleIndex = 1;
            this.gcolTenLoiLoc.Width = 354;
            // 
            // gvcolThoiGianThayLoi
            // 
            this.gvcolThoiGianThayLoi.Caption = "Thời gian thay lõi";
            this.gvcolThoiGianThayLoi.ColumnEdit = this.grThayGianThayThe;
            this.gvcolThoiGianThayLoi.FieldName = "ThoiGianThayLoi";
            this.gvcolThoiGianThayLoi.Name = "gvcolThoiGianThayLoi";
            this.gvcolThoiGianThayLoi.OptionsColumn.AllowEdit = false;
            this.gvcolThoiGianThayLoi.Visible = true;
            this.gvcolThoiGianThayLoi.VisibleIndex = 2;
            this.gvcolThoiGianThayLoi.Width = 204;
            // 
            // grThayGianThayThe
            // 
            this.grThayGianThayThe.AutoHeight = false;
            this.grThayGianThayThe.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.grThayGianThayThe.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Mã", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Tháng Thay Lõi")});
            this.grThayGianThayThe.DisplayMember = "Name";
            this.grThayGianThayThe.KeyMember = "Code";
            this.grThayGianThayThe.Name = "grThayGianThayThe";
            this.grThayGianThayThe.NullText = "";
            this.grThayGianThayThe.ShowFooter = false;
            this.grThayGianThayThe.ValueMember = "Code";
            // 
            // gvGhiChu
            // 
            this.gvGhiChu.Caption = "Ghi Chú";
            this.gvGhiChu.FieldName = "GhiChu";
            this.gvGhiChu.Name = "gvGhiChu";
            this.gvGhiChu.OptionsColumn.AllowEdit = false;
            this.gvGhiChu.Visible = true;
            this.gvGhiChu.VisibleIndex = 3;
            this.gvGhiChu.Width = 263;
            // 
            // txtghiChu
            // 
            this.txtghiChu.Location = new System.Drawing.Point(209, 91);
            this.txtghiChu.Multiline = true;
            this.txtghiChu.Name = "txtghiChu";
            this.txtghiChu.Size = new System.Drawing.Size(402, 62);
            this.txtghiChu.TabIndex = 9;
            // 
            // chkcbLoaiMay
            // 
            this.chkcbLoaiMay.EditValue = ((object)(resources.GetObject("chkcbLoaiMay.EditValue")));
            this.chkcbLoaiMay.Location = new System.Drawing.Point(209, 55);
            this.chkcbLoaiMay.Name = "chkcbLoaiMay";
            this.chkcbLoaiMay.Properties.AllowMultiSelect = true;
            this.chkcbLoaiMay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.chkcbLoaiMay.Properties.DisplayMember = "TenLoaiMay";
            this.chkcbLoaiMay.Properties.EditValueType = DevExpress.XtraEditors.Repository.EditValueTypeCollection.List;
            this.chkcbLoaiMay.Properties.NullText = "Vui lòng chọn chủng loại";
            this.chkcbLoaiMay.Properties.NullValuePrompt = "Vui lòng chọn chủng loại";
            this.chkcbLoaiMay.Properties.NullValuePromptShowForEmptyValue = true;
            this.chkcbLoaiMay.Properties.SelectAllItemCaption = "Select All";
            this.chkcbLoaiMay.Properties.SelectAllItemVisible = false;
            this.chkcbLoaiMay.Properties.ShowButtons = false;
            this.chkcbLoaiMay.Properties.ShowPopupCloseButton = false;
            this.chkcbLoaiMay.Properties.ShowPopupShadow = false;
            this.chkcbLoaiMay.Properties.ValueMember = "MaLoaiMay";
            this.chkcbLoaiMay.Size = new System.Drawing.Size(240, 20);
            this.chkcbLoaiMay.TabIndex = 8;
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapNhat.Appearance.Options.UseFont = true;
            this.btnCapNhat.Location = new System.Drawing.Point(628, 91);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(228, 62);
            this.btnCapNhat.TabIndex = 5;
            this.btnCapNhat.Text = "Lưu";
            this.btnCapNhat.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtTenLoi
            // 
            this.txtTenLoi.Location = new System.Drawing.Point(209, 15);
            this.txtTenLoi.Name = "txtTenLoi";
            this.txtTenLoi.Size = new System.Drawing.Size(647, 21);
            this.txtTenLoi.TabIndex = 1;
            // 
            // LbGhiChu
            // 
            this.LbGhiChu.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbGhiChu.Location = new System.Drawing.Point(124, 91);
            this.LbGhiChu.Name = "LbGhiChu";
            this.LbGhiChu.Size = new System.Drawing.Size(76, 23);
            this.LbGhiChu.TabIndex = 0;
            this.LbGhiChu.Text = "Ghi Chú:";
            this.LbGhiChu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.LbGhiChu.Click += new System.EventHandler(this.LbGhiChu_Click);
            // 
            // lbLoaiLoi
            // 
            this.lbLoaiLoi.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLoaiLoi.Location = new System.Drawing.Point(69, 50);
            this.lbLoaiLoi.Name = "lbLoaiLoi";
            this.lbLoaiLoi.Size = new System.Drawing.Size(131, 23);
            this.lbLoaiLoi.TabIndex = 0;
            this.lbLoaiLoi.Text = "Thuộc loại máy :";
            this.lbLoaiLoi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbTGTu
            // 
            this.lbTGTu.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTGTu.Location = new System.Drawing.Point(462, 52);
            this.lbTGTu.Name = "lbTGTu";
            this.lbTGTu.Size = new System.Drawing.Size(160, 23);
            this.lbTGTu.TabIndex = 0;
            this.lbTGTu.Text = "Thời gian thay lõi :";
            this.lbTGTu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbTenLoi
            // 
            this.lbTenLoi.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTenLoi.Location = new System.Drawing.Point(76, 15);
            this.lbTenLoi.Name = "lbTenLoi";
            this.lbTenLoi.Size = new System.Drawing.Size(124, 23);
            this.lbTenLoi.TabIndex = 0;
            this.lbTenLoi.Text = "Tên sản phẩm :";
            this.lbTenLoi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbTGThayLoi
            // 
            this.cbTGThayLoi.Location = new System.Drawing.Point(628, 54);
            this.cbTGThayLoi.Name = "cbTGThayLoi";
            this.cbTGThayLoi.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbTGThayLoi.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Thời Gian"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Mã", 10, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default)});
            this.cbTGThayLoi.Properties.DisplayMember = "Name";
            this.cbTGThayLoi.Properties.NullText = "";
            this.cbTGThayLoi.Properties.PopupSizeable = false;
            this.cbTGThayLoi.Properties.ShowFooter = false;
            this.cbTGThayLoi.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbTGThayLoi.Properties.ValueMember = "Code";
            this.cbTGThayLoi.Size = new System.Drawing.Size(228, 20);
            this.cbTGThayLoi.TabIndex = 4;
            // 
            // cbThemMoiLoiLoc
            // 
            this.cbThemMoiLoiLoc.AutoSize = true;
            this.cbThemMoiLoiLoc.Location = new System.Drawing.Point(209, 167);
            this.cbThemMoiLoiLoc.Name = "cbThemMoiLoiLoc";
            this.cbThemMoiLoiLoc.Size = new System.Drawing.Size(100, 17);
            this.cbThemMoiLoiLoc.TabIndex = 30;
            this.cbThemMoiLoiLoc.Text = "Thêm mới lõi lọc";
            this.cbThemMoiLoiLoc.UseVisualStyleBackColor = true;
            this.cbThemMoiLoiLoc.CheckedChanged += new System.EventHandler(this.cbThemMoiLoiLoc_CheckedChanged);
            // 
            // frmThemLoiLocMoi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "frmThemLoiLocMoi";
            this.ShowIcon = false;
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdLMNL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLMNL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grThayGianThayThe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkcbLoaiMay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTGThayLoi.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.Label lb_name;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btnCapNhat;
        private System.Windows.Forms.Label LbGhiChu;
        private System.Windows.Forms.Label lbLoaiLoi;
        private System.Windows.Forms.Label lbTGTu;
        private System.Windows.Forms.Label lbTenLoi;
        private System.Windows.Forms.TextBox txtTenLoi;
        private DevExpress.XtraEditors.LookUpEdit cbTGThayLoi;
        private DevExpress.XtraEditors.CheckedComboBoxEdit chkcbLoaiMay;
        private System.Windows.Forms.TextBox txtghiChu;
        private DevExpress.XtraGrid.GridControl grdLMNL;
        private DevExpress.XtraGrid.Views.Grid.GridView gvLMNL;
        private DevExpress.XtraGrid.Columns.GridColumn gvcolMaLoiLoc;
        private DevExpress.XtraGrid.Columns.GridColumn gcolTenLoiLoc;
        private DevExpress.XtraGrid.Columns.GridColumn gvGhiChu;
        private DevExpress.XtraGrid.Columns.GridColumn gvcolThoiGianThayLoi;
        public DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit grThayGianThayThe;
		private System.Windows.Forms.CheckBox cbThemMoiLoiLoc;

    }
}