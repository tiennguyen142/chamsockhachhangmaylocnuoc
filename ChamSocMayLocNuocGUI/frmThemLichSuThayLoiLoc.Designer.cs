﻿namespace ChamSocMayLocNuoc
{
    partial class frmThemLichSuThayLoiLoc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lb_name = new System.Windows.Forms.Label();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.cldNgayMua = new DevExpress.XtraEditors.DateEdit();
            this.lbNgayThay = new System.Windows.Forms.Label();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.txtTenKH = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMaKH = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtGhiChu = new System.Windows.Forms.TextBox();
            this.lbGhiChu = new System.Windows.Forms.Label();
            this.chkbLoaiLoi = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cldNgayMua.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cldNgayMua.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkbLoaiLoi.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Controls.Add(this.lb_name);
            this.panelControl1.Location = new System.Drawing.Point(-2, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(489, 38);
            this.panelControl1.TabIndex = 14;
            // 
            // lb_name
            // 
            this.lb_name.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_name.Location = new System.Drawing.Point(4, 7);
            this.lb_name.Name = "lb_name";
            this.lb_name.Size = new System.Drawing.Size(450, 26);
            this.lb_name.TabIndex = 1;
            this.lb_name.Text = "Thêm Lịch Sử Thay Lõi";
            this.lb_name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.Controls.Add(this.cldNgayMua);
            this.panelControl2.Controls.Add(this.lbNgayThay);
            this.panelControl2.Controls.Add(this.btnLuu);
            this.panelControl2.Controls.Add(this.txtTenKH);
            this.panelControl2.Controls.Add(this.label2);
            this.panelControl2.Controls.Add(this.txtMaKH);
            this.panelControl2.Controls.Add(this.label1);
            this.panelControl2.Controls.Add(this.label4);
            this.panelControl2.Controls.Add(this.txtGhiChu);
            this.panelControl2.Controls.Add(this.lbGhiChu);
            this.panelControl2.Controls.Add(this.chkbLoaiLoi);
            this.panelControl2.Location = new System.Drawing.Point(-2, 40);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(489, 294);
            this.panelControl2.TabIndex = 14;
            // 
            // cldNgayMua
            // 
            this.cldNgayMua.EditValue = null;
            this.cldNgayMua.Location = new System.Drawing.Point(160, 82);
            this.cldNgayMua.Name = "cldNgayMua";
            this.cldNgayMua.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cldNgayMua.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cldNgayMua.Properties.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.cldNgayMua.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.cldNgayMua.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.cldNgayMua.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.cldNgayMua.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.cldNgayMua.Size = new System.Drawing.Size(294, 20);
            this.cldNgayMua.TabIndex = 31;
            // 
            // lbNgayThay
            // 
            this.lbNgayThay.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNgayThay.Location = new System.Drawing.Point(12, 77);
            this.lbNgayThay.Name = "lbNgayThay";
            this.lbNgayThay.Size = new System.Drawing.Size(100, 23);
            this.lbNgayThay.TabIndex = 28;
            this.lbNgayThay.Text = "Ngày Thay :";
            this.lbNgayThay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnLuu
            // 
            this.btnLuu.Location = new System.Drawing.Point(190, 244);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(114, 39);
            this.btnLuu.TabIndex = 27;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // txtTenKH
            // 
            this.txtTenKH.Location = new System.Drawing.Point(160, 48);
            this.txtTenKH.Name = "txtTenKH";
            this.txtTenKH.Size = new System.Drawing.Size(294, 21);
            this.txtTenKH.TabIndex = 24;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(137, 23);
            this.label2.TabIndex = 21;
            this.label2.Text = "Tên Khách Hàng :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMaKH
            // 
            this.txtMaKH.Location = new System.Drawing.Point(160, 16);
            this.txtMaKH.Name = "txtMaKH";
            this.txtMaKH.Size = new System.Drawing.Size(295, 21);
            this.txtMaKH.TabIndex = 24;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 23);
            this.label1.TabIndex = 21;
            this.label1.Text = "Mã Khách Hàng :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(9, 111);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(124, 23);
            this.label4.TabIndex = 21;
            this.label4.Text = "Loại Lõi Lọc :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Location = new System.Drawing.Point(160, 152);
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(295, 77);
            this.txtGhiChu.TabIndex = 25;
            // 
            // lbGhiChu
            // 
            this.lbGhiChu.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGhiChu.Location = new System.Drawing.Point(9, 153);
            this.lbGhiChu.Name = "lbGhiChu";
            this.lbGhiChu.Size = new System.Drawing.Size(123, 23);
            this.lbGhiChu.TabIndex = 23;
            this.lbGhiChu.Text = "Nội Dung :";
            this.lbGhiChu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // chkbLoaiLoi
            // 
            this.chkbLoaiLoi.Location = new System.Drawing.Point(160, 115);
            this.chkbLoaiLoi.Name = "chkbLoaiLoi";
            this.chkbLoaiLoi.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.chkbLoaiLoi.Properties.DisplayMember = "TenLoiLoc";
            this.chkbLoaiLoi.Properties.SelectAllItemVisible = false;
            this.chkbLoaiLoi.Properties.ShowButtons = false;
            this.chkbLoaiLoi.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.chkbLoaiLoi.Properties.ValueMember = "MaLoiLoc";
            this.chkbLoaiLoi.Size = new System.Drawing.Size(294, 20);
            this.chkbLoaiLoi.TabIndex = 30;
            // 
            // frmThemLichSuThayLoiLoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(486, 335);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "frmThemLichSuThayLoiLoc";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cldNgayMua.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cldNgayMua.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkbLoaiLoi.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.Label lb_name;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbNgayThay;
        private System.Windows.Forms.TextBox txtGhiChu;
        private System.Windows.Forms.Label lbGhiChu;
        private DevExpress.XtraEditors.DateEdit cldNgayMua;
        private System.Windows.Forms.TextBox txtMaKH;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckedComboBoxEdit chkbLoaiLoi;
        private System.Windows.Forms.TextBox txtTenKH;
        private System.Windows.Forms.Label label2;
    }
}