﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ChamSocMayLocNuocDAL.Model;
namespace ChamSocMayLocNuoc
{
    public partial class frmManHinhChinh : DevExpress.XtraEditors.XtraForm
    {
        public frmManHinhChinh()
        {
            InitializeComponent();
            Showfrm();
        }
        public void Showfrm()
        {
           
            
        }

        private void btnThemKH_Click(object sender, EventArgs e)
        {
            frmThemKhachHang frmThKH = new frmThemKhachHang();
            frmThKH.Show();
        }

        private void btnTraCuuKH_Click(object sender, EventArgs e)
        {
            frmTraCuuKhachHang frmTraCuu = new frmTraCuuKhachHang();
            frmTraCuu.Show();
        }

        private void btnLSNN_Click(object sender, EventArgs e)
        {
            frmLichSuNhacNho frmLSNN = new frmLichSuNhacNho();
            frmLSNN.Show();
        }

        private void btnTSP_Click(object sender, EventArgs e)
        {
            frmThemLoaiMayLoc frmTHSP = new frmThemLoaiMayLoc();
            frmTHSP.Show();
        }

        private void btnCauHinh_Click(object sender, EventArgs e)
        {
            frmCauHinh frmCauHinh = new frmCauHinh();
            frmCauHinh.Show();
        }

        //private void simpleButton6_Click(object sender, EventArgs e)
        //{
        //    frmNhacNho frmNN = new frmNhacNho();
        //    frmNN.Show();
        //}

        private void button1_Click(object sender, EventArgs e)
        {
            frmThemLoiLocMoi frmLLMoi = new frmThemLoiLocMoi();
            frmLLMoi.Show();
        }

        private void btnNhacNho_Click(object sender, EventArgs e)
        {
            frmNhacNho frmNN = new frmNhacNho();
            frmNN.Show();
        }
    }
}
