﻿namespace ChamSocMayLocNuoc
{
    partial class frmLichSuThayLoi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.label1 = new System.Windows.Forms.Label();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbSDT = new System.Windows.Forms.Label();
            this.lbTenKH = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbTGBaoHanConLai = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.grdLSBH = new DevExpress.XtraGrid.GridControl();
            this.gvLSBH = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gvcolNgayThayLoi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolNDung = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvcolmaLsThayLoi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvcolKh = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdLSBH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLSBH)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Controls.Add(this.label1);
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(899, 53);
            this.panelControl1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(226, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(447, 40);
            this.label1.TabIndex = 1;
            this.label1.Text = "Xem Lịch Sử Thay Lõi";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.Controls.Add(this.groupBox1);
            this.panelControl2.Controls.Add(this.grdLSBH);
            this.panelControl2.Location = new System.Drawing.Point(0, 53);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(899, 476);
            this.panelControl2.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbSDT);
            this.groupBox1.Controls.Add(this.lbTenKH);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lbTGBaoHanConLai);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(5, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(889, 90);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông Tin Khách Hàng";
            // 
            // lbSDT
            // 
            this.lbSDT.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSDT.Location = new System.Drawing.Point(152, 59);
            this.lbSDT.Name = "lbSDT";
            this.lbSDT.Size = new System.Drawing.Size(248, 22);
            this.lbSDT.TabIndex = 4;
            this.lbSDT.Text = "016732232";
            this.lbSDT.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbTenKH
            // 
            this.lbTenKH.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTenKH.Location = new System.Drawing.Point(152, 21);
            this.lbTenKH.Name = "lbTenKH";
            this.lbTenKH.Size = new System.Drawing.Size(248, 22);
            this.lbTenKH.TabIndex = 4;
            this.lbTenKH.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(11, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 24);
            this.label3.TabIndex = 0;
            this.label3.Text = "Tên Khách Hàng :";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(11, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(135, 27);
            this.label2.TabIndex = 0;
            this.label2.Text = "Số Điện Thoại :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbTGBaoHanConLai
            // 
            this.lbTGBaoHanConLai.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTGBaoHanConLai.Location = new System.Drawing.Point(634, 23);
            this.lbTGBaoHanConLai.Name = "lbTGBaoHanConLai";
            this.lbTGBaoHanConLai.Size = new System.Drawing.Size(206, 22);
            this.lbTGBaoHanConLai.TabIndex = 0;
            this.lbTGBaoHanConLai.Text = "1 năm 2 tháng";
            this.lbTGBaoHanConLai.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(406, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(222, 22);
            this.label4.TabIndex = 0;
            this.label4.Text = "Thời gian bảo hành còn lại :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // grdLSBH
            // 
            this.grdLSBH.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.grdLSBH.Location = new System.Drawing.Point(5, 102);
            this.grdLSBH.MainView = this.gvLSBH;
            this.grdLSBH.Name = "grdLSBH";
            this.grdLSBH.Size = new System.Drawing.Size(894, 374);
            this.grdLSBH.TabIndex = 0;
            this.grdLSBH.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvLSBH});
            // 
            // gvLSBH
            // 
            this.gvLSBH.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gvLSBH.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvLSBH.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gvcolNgayThayLoi,
            this.gcolNDung,
            this.gvcolmaLsThayLoi,
            this.gvcolKh});
            this.gvLSBH.GridControl = this.grdLSBH;
            this.gvLSBH.Name = "gvLSBH";
            this.gvLSBH.OptionsFilter.AllowAutoFilterConditionChange = DevExpress.Utils.DefaultBoolean.True;
            this.gvLSBH.OptionsView.ShowAutoFilterRow = true;
            this.gvLSBH.OptionsView.ShowGroupPanel = false;
            this.gvLSBH.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gvLSBH_RowClick);
            // 
            // gvcolNgayThayLoi
            // 
            this.gvcolNgayThayLoi.Caption = "Ngày Thay Lõi";
            this.gvcolNgayThayLoi.FieldName = "NgayMua";
            this.gvcolNgayThayLoi.Name = "gvcolNgayThayLoi";
            this.gvcolNgayThayLoi.OptionsColumn.AllowEdit = false;
            this.gvcolNgayThayLoi.Visible = true;
            this.gvcolNgayThayLoi.VisibleIndex = 1;
            this.gvcolNgayThayLoi.Width = 157;
            // 
            // gcolNDung
            // 
            this.gcolNDung.Caption = "Nội Dung";
            this.gcolNDung.FieldName = "GhiChu";
            this.gcolNDung.Name = "gcolNDung";
            this.gcolNDung.OptionsColumn.AllowEdit = false;
            this.gcolNDung.Visible = true;
            this.gcolNDung.VisibleIndex = 3;
            this.gcolNDung.Width = 154;
            // 
            // gvcolmaLsThayLoi
            // 
            this.gvcolmaLsThayLoi.Caption = "Mã Lịch Sử Thay Lõi";
            this.gvcolmaLsThayLoi.Name = "gvcolmaLsThayLoi";
            this.gvcolmaLsThayLoi.Visible = true;
            this.gvcolmaLsThayLoi.VisibleIndex = 0;
            this.gvcolmaLsThayLoi.Width = 166;
            // 
            // gvcolKh
            // 
            this.gvcolKh.Caption = "Tên Khách Hàng";
            this.gvcolKh.Name = "gvcolKh";
            this.gvcolKh.Visible = true;
            this.gvcolKh.VisibleIndex = 2;
            this.gvcolKh.Width = 101;
            // 
            // frmLichSuThayLoi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(899, 529);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "frmLichSuThayLoi";
            this.ShowIcon = false;
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdLSBH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLSBH)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraGrid.GridControl grdLSBH;
        private DevExpress.XtraGrid.Views.Grid.GridView gvLSBH;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbSDT;
        private System.Windows.Forms.Label lbTenKH;
        private System.Windows.Forms.Label lbTGBaoHanConLai;
        private DevExpress.XtraGrid.Columns.GridColumn gcolNDung;
        private DevExpress.XtraGrid.Columns.GridColumn gvcolNgayThayLoi;
        private DevExpress.XtraGrid.Columns.GridColumn gvcolmaLsThayLoi;
        private DevExpress.XtraGrid.Columns.GridColumn gvcolKh;
    }
}