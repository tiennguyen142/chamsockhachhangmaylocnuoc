﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ChamSocMayLocNuocDAL.Model;

namespace ChamSocMayLocNuoc
{
    public partial class frmLichSuThayLoi : DevExpress.XtraEditors.XtraForm
    {
        ChamSocMayLocNuocDbContext dbc = new ChamSocMayLocNuocDbContext();
        public frmLichSuThayLoi()
        {
            InitializeComponent();
            LoadLichSuThayLoi();
        }

        private void btnThemLSThayLoi_Click(object sender, EventArgs e)
        {
          //  frmThemLichSuThayLoiLoc frm = new frmThemLichSuThayLoiLoc();
          //  frm.Show();
        }
        public void LoadLichSuThayLoi()
        {
        
            grdLSBH.DataSource = dbc.LichSuThayLoiLocs.ToList(); ;
        }

        private void gvLSBH_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
           LichSuThayLoiLoc lichSuThayloi = new LichSuThayLoiLoc();
            lichSuThayloi = gvLSBH.GetRow(e.RowHandle) as LichSuThayLoiLoc;

            LoiLoc LL = dbc.LoiLocs.ToList().Find(s=>s.MaLoiLoc==lichSuThayloi.MaLoiLoc);
            KhachHang kh = dbc.KhachHangs.ToList().Find(s => s.MaKH == lichSuThayloi.MaKH) as KhachHang;
            lbSDT.Text = kh.SDT;
            lbTenKH.Text = kh.TenKH;
            TimeSpan days = DateTime.Today.Subtract(lichSuThayloi.NgayMua.AddMonths(LL.ThoiGianThayLoi));
            lbTGBaoHanConLai.Text = (days.Days/30).ToString();
        }
    }
}