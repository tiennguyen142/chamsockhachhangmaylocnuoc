﻿namespace ChamSocMayLocNuoc
{
    partial class frmNhacNho
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNhacNho));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lb_name = new System.Windows.Forms.Label();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.grdThongBao = new DevExpress.XtraGrid.GridControl();
            this.gvThongBao = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gvcolMaKH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolTenKH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvColSDT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvDiaChi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvNgayMua = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gvcolThoiGianThayLoi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grThoiGianThayLoi = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.girdDagoi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.girdDaxem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdThongBao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvThongBao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grThoiGianThayLoi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Controls.Add(this.lb_name);
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1186, 38);
            this.panelControl1.TabIndex = 0;
            // 
            // lb_name
            // 
            this.lb_name.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_name.Location = new System.Drawing.Point(0, 6);
            this.lb_name.Name = "lb_name";
            this.lb_name.Size = new System.Drawing.Size(1181, 26);
            this.lb_name.TabIndex = 2;
            this.lb_name.Text = "Thông Báo Tới Thời Gian Thay Lõi";
            this.lb_name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.Controls.Add(this.grdThongBao);
            this.panelControl2.Location = new System.Drawing.Point(0, 35);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1186, 528);
            this.panelControl2.TabIndex = 0;
            // 
            // grdThongBao
            // 
            this.grdThongBao.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdThongBao.Location = new System.Drawing.Point(0, 0);
            this.grdThongBao.MainView = this.gvThongBao;
            this.grdThongBao.Name = "grdThongBao";
            this.grdThongBao.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.grThoiGianThayLoi,
            this.repositoryItemDateEdit1});
            this.grdThongBao.Size = new System.Drawing.Size(1181, 528);
            this.grdThongBao.TabIndex = 30;
            this.grdThongBao.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvThongBao});
            // 
            // gvThongBao
            // 
            this.gvThongBao.ActiveFilterEnabled = false;
            this.gvThongBao.Appearance.FocusedRow.BackColor = System.Drawing.Color.Transparent;
            this.gvThongBao.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.Transparent;
            this.gvThongBao.Appearance.FocusedRow.BorderColor = System.Drawing.Color.Transparent;
            this.gvThongBao.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvThongBao.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvThongBao.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gvThongBao.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvThongBao.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gvcolMaKH,
            this.gcolTenKH,
            this.gvColSDT,
            this.gvDiaChi,
            this.gvNgayMua,
            this.gvcolThoiGianThayLoi,
            this.girdDagoi,
            this.girdDaxem});
            this.gvThongBao.GridControl = this.grdThongBao;
            this.gvThongBao.Name = "gvThongBao";
            this.gvThongBao.OptionsCustomization.AllowFilter = false;
            this.gvThongBao.OptionsFilter.AllowAutoFilterConditionChange = DevExpress.Utils.DefaultBoolean.True;
            this.gvThongBao.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gvThongBao.OptionsFilter.AllowFilterEditor = false;
            this.gvThongBao.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gvThongBao.OptionsFilter.AllowMRUFilterList = false;
            this.gvThongBao.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gvThongBao.OptionsFilter.ColumnFilterPopupMode = DevExpress.XtraGrid.Columns.ColumnFilterPopupMode.Classic;
            this.gvThongBao.OptionsFilter.DefaultFilterEditorView = DevExpress.XtraEditors.FilterEditorViewMode.Text;
            this.gvThongBao.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gvThongBao.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gvThongBao.OptionsFilter.ShowAllTableValuesInFilterPopup = true;
            this.gvThongBao.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gvThongBao.OptionsView.ShowAutoFilterRow = true;
            this.gvThongBao.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gvThongBao.OptionsView.ShowGroupPanel = false;
            this.gvThongBao.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.gvThongBao_CustomDrawRowIndicator);
            this.gvThongBao.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gvThongBao_RowCellStyle);
            this.gvThongBao.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gvThongBao_RowStyle);
            this.gvThongBao.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvThongBao_CellValueChanged);
            // 
            // gvcolMaKH
            // 
            this.gvcolMaKH.Caption = "Mã Khách Hàng";
            this.gvcolMaKH.FieldName = "MaKhachHang";
            this.gvcolMaKH.Name = "gvcolMaKH";
            this.gvcolMaKH.OptionsColumn.AllowEdit = false;
            this.gvcolMaKH.OptionsFilter.AllowFilterModeChanging = DevExpress.Utils.DefaultBoolean.False;
            this.gvcolMaKH.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gvcolMaKH.Visible = true;
            this.gvcolMaKH.VisibleIndex = 0;
            this.gvcolMaKH.Width = 128;
            // 
            // gcolTenKH
            // 
            this.gcolTenKH.Caption = "Tên Khách Hàng";
            this.gcolTenKH.FieldName = "TenKhachHang";
            this.gcolTenKH.Name = "gcolTenKH";
            this.gcolTenKH.OptionsColumn.AllowEdit = false;
            this.gcolTenKH.Visible = true;
            this.gcolTenKH.VisibleIndex = 1;
            this.gcolTenKH.Width = 250;
            // 
            // gvColSDT
            // 
            this.gvColSDT.Caption = "Số Điện Thoại";
            this.gvColSDT.FieldName = "SoDienThoai";
            this.gvColSDT.Name = "gvColSDT";
            this.gvColSDT.OptionsColumn.AllowEdit = false;
            this.gvColSDT.Visible = true;
            this.gvColSDT.VisibleIndex = 3;
            this.gvColSDT.Width = 151;
            // 
            // gvDiaChi
            // 
            this.gvDiaChi.Caption = "Địa Chỉ";
            this.gvDiaChi.FieldName = "DiaChi";
            this.gvDiaChi.Name = "gvDiaChi";
            this.gvDiaChi.OptionsColumn.AllowEdit = false;
            this.gvDiaChi.Visible = true;
            this.gvDiaChi.VisibleIndex = 2;
            this.gvDiaChi.Width = 259;
            // 
            // gvNgayMua
            // 
            this.gvNgayMua.Caption = "Ngày Mua";
            this.gvNgayMua.ColumnEdit = this.repositoryItemDateEdit1;
            this.gvNgayMua.FieldName = "NgayMua";
            this.gvNgayMua.Name = "gvNgayMua";
            this.gvNgayMua.OptionsColumn.AllowEdit = false;
            this.gvNgayMua.Visible = true;
            this.gvNgayMua.VisibleIndex = 4;
            this.gvNgayMua.Width = 171;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.EditFormat.FormatString = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Mask.EditMask = "dd/MM/yyyy";
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // gvcolThoiGianThayLoi
            // 
            this.gvcolThoiGianThayLoi.Caption = "Thời gian thay lõi (Tháng)";
            this.gvcolThoiGianThayLoi.ColumnEdit = this.grThoiGianThayLoi;
            this.gvcolThoiGianThayLoi.FieldName = "ThoiGianThayLoi";
            this.gvcolThoiGianThayLoi.Name = "gvcolThoiGianThayLoi";
            this.gvcolThoiGianThayLoi.OptionsColumn.AllowEdit = false;
            this.gvcolThoiGianThayLoi.Visible = true;
            this.gvcolThoiGianThayLoi.VisibleIndex = 5;
            this.gvcolThoiGianThayLoi.Width = 204;
            // 
            // grThoiGianThayLoi
            // 
            this.grThoiGianThayLoi.AutoHeight = false;
            this.grThoiGianThayLoi.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.grThoiGianThayLoi.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Mã"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "ThoiGianThayLoi")});
            this.grThoiGianThayLoi.DisplayMember = "Name";
            this.grThoiGianThayLoi.Name = "grThoiGianThayLoi";
            this.grThoiGianThayLoi.ValueMember = "Code";
            // 
            // girdDagoi
            // 
            this.girdDagoi.Caption = "Đã Thay";
            this.girdDagoi.FieldName = "DaGoi";
            this.girdDagoi.Name = "girdDagoi";
            this.girdDagoi.Visible = true;
            this.girdDagoi.VisibleIndex = 6;
            // 
            // girdDaxem
            // 
            this.girdDaxem.Caption = "Đã Gọi";
            this.girdDaxem.FieldName = "DaXem";
            this.girdDaxem.Name = "girdDaxem";
            this.girdDaxem.Visible = true;
            this.girdDaxem.VisibleIndex = 7;
            // 
            // frmNhacNho
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 561);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.HtmlText = "Nhắc Nhở";
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmNhacNho";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdThongBao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvThongBao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grThoiGianThayLoi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private System.Windows.Forms.Label lb_name;
        private DevExpress.XtraGrid.GridControl grdThongBao;
        private DevExpress.XtraGrid.Views.Grid.GridView gvThongBao;
        private DevExpress.XtraGrid.Columns.GridColumn gvcolMaKH;
        private DevExpress.XtraGrid.Columns.GridColumn gcolTenKH;
        private DevExpress.XtraGrid.Columns.GridColumn gvcolThoiGianThayLoi;
        private DevExpress.XtraGrid.Columns.GridColumn gvDiaChi;
        private DevExpress.XtraGrid.Columns.GridColumn gvColSDT;
        private DevExpress.XtraGrid.Columns.GridColumn gvNgayMua;
        private DevExpress.XtraGrid.Columns.GridColumn girdDagoi;
        private DevExpress.XtraGrid.Columns.GridColumn girdDaxem;
        public DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit grThoiGianThayLoi;
        private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
    }
}